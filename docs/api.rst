..
    Copyright (C) 2018 CERN.

    publishing is free software; you can redistribute it and/or modify it
    under the terms of the MIT License; see LICENSE file for more details.


API Docs
========

Views
-----

.. automodule:: publishing.views
   :members:
