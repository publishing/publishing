..
    Copyright (C) 2018 CERN.

    publishing is free software; you can redistribute it and/or modify it
    under the terms of the MIT License; see LICENSE file for more details.

============
 publishing
============

.. image:: https://img.shields.io/travis/publishing/publishing.svg
        :target: https://travis-ci.org/publishing/publishing

.. image:: https://img.shields.io/coveralls/publishing/publishing.svg
        :target: https://coveralls.io/r/publishing/publishing

.. image:: https://img.shields.io/github/license/publishing/publishing.svg
        :target: https://github.com/publishing/publishing/blob/master/LICENSE

Invenio digital library framework.

Further documentation is available on
https://publishing.readthedocs.io/

