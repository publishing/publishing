"""JS/CSS Webpack bundles for theme."""

from flask_webpackext import WebpackBundle

theme = WebpackBundle(
    __name__,
    'assets',
    entry={
        'publishing-theme': './scss/publishing/theme.scss',
    },
    dependencies={
        'bootstrap-sass': '~3.3.5',
        'bootstrap-switch': '~3.0.2',
        'font-awesome': '~4.4.0',
        'typeahead.js-bootstrap-css': '~1.2.1',
    }
)


stats_js = WebpackBundle(
    __name__,
    'assets',
    entry={
        'publishing-stats-js': './js/publishing/stats.js'
    },
    dependencies={
        'chart.js': '~2.8.0'
    }
)


search_js = WebpackBundle(
    __name__,
    'assets',
    entry={
        'publishing-search-js': './js/publishing/search.js'
    },
    dependencies={
        'almond': '~0.3.1',
        'angular': '~1.4.10',
        'angular-loading-bar': '~0.9.0',
        'd3': '^3.5.17',
        'invenio-search-js': '^1.3.1',
        'jquery': '~3.2.1',
    }
)


citation_js = WebpackBundle(
    __name__,
    'assets',
    entry={
        'publishing-citations-js': './js/publishing/citations.js'
    },
    dependencies={
        'angular': '~1.4.10',
        'invenio-csl-js': '~0.1.3',
        'typeahead.js': '~0.11.1',
        'bloodhound-js':'~1.2.3',
        'jquery': '~3.2.1',
    }
)