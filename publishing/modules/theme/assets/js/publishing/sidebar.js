import $ from 'jquery';

$('.open_btn').click(function(e){
    e.stopPropagation();
    $('div.recordtabs').addClass('expanded');
    $('div.recordtabs').animate({width:"195px"}, 300 );

    });

function close_menu(){
    if ($('div.recordtabs').hasClass('expanded')) {
        $('div.recordtabs').animate({width:"0"}, 300 );
        $('div.recordtabs').removeClass('expanded',300);
    }
}
    
$(document.body).click( function(e) {
    close_menu();
    });

$("div.recordtabs").click( function(e) {
    e.stopPropagation();
});
