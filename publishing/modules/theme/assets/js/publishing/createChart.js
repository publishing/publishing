
window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};



export function getStats(views, months, downloads) {

    const config = {
        type: 'bar',
        data: {
            labels: months,
            datasets: [{
                label: 'Record Views',
                fill: false,
                backgroundColor: window.chartColors.blue,
                borderColor: window.chartColors.blue,
                data: views,
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            title: {
                display: false,
                text: 'Record Statistics'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Months',
                    },
                    maxBarThickness: 100 // number (pixels)
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Hits'
                    },
                    ticks: {
                        beginAtZero:true,
                        precision:0
                    }
                }]
            }
        }
    };

    if (downloads.length !== 0) {
        config.data.datasets.push(
            {
                label: 'Record Downloads',
                fill: false,
                backgroundColor: window.chartColors.red,
				borderColor: window.chartColors.red,
                data: downloads,
            }
        )
        
    };


    fetchRecordData('downloads', config); // fetch the pageviews for a record
}

/* eslint-disable */

function fetchRecordData(category, config) {

    var ctx = document.getElementById('StatsChart').getContext('2d');

    new Chart(ctx, config);
  }
/* eslint-enable */
