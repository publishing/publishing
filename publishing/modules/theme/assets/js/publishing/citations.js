import angular from "angular";
import "invenio-csl-js/dist/invenio-csl-js.js";
import "typeahead.js/dist/typeahead.bundle.js";
import "jquery/dist/jquery.js";
import  Bloodhound  from "typeahead.js/dist/bloodhound";

global.Bloodhound = Bloodhound;

angular.element(document).ready(function() {
    angular.bootstrap(document.getElementById("invenio-csl"), [
      'invenioCsl',
    ]);
  });
