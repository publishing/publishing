import angular from "angular";
import StripTags from './striptags';


export default angular.module('publishingFilters', [])
    .filter('stripTags', StripTags).name;


