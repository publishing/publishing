export default function StripTags() {
    return function(text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
  };
