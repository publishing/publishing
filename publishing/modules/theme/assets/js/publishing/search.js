import angular from "angular";
import "angular-loading-bar";
import "invenio-search-js/dist/invenio-search-js";
import "./module"

angular.element(document).ready(function() {
    angular.bootstrap(document.getElementById("invenio-search"), [
      "angular-loading-bar",
      "invenioSearch",
      'publishing',
    ]);
  });
