"""Theme blueprint in order for template and static files to be loaded."""

from __future__ import absolute_import, print_function

from flask import Blueprint

blueprint = Blueprint(
    'publishing_theme',
    __name__,
    template_folder='templates',
    static_folder='assets',
)
"""Theme blueprint used to define template and static folders."""