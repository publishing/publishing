from flask_iiif.restful import IIIFImageAPI
from invenio_files_rest.models import ObjectVersion
from invenio_iiif.utils import ui_iiif_image_url
from publishing.modules.records.api import PublishingRecord



def get_cover_image(record):
    "Get Cover Image for records"

    bucket = record.files.bucket
    key = None
    image_url = None

    for f in record.get('_files',[]):
        if f.get('galley_type') == "Cover":
            key = f['key']

    if key:
        obj = ObjectVersion.get(bucket, key)

        image_url = ui_iiif_image_url(obj=obj,version='v2', region='full', size='full',
                                rotation=0,quality='default', image_format='png')

    return image_url


def get_thumb_image(record_files, thumbnail_size=120):
    "Create thumbnail for  cover"

    for f in record_files:
        f = f.to_dict()
        if f.get('galley_type') == "Cover":
            try:
                return ui_iiif_image_url(
                            f,
                            size='{},'.format(thumbnail_size),
                            image_format='png' if f['type'] == 'png' else 'jpg',
                        )
            except RuntimeError:
                pass
            break


            

