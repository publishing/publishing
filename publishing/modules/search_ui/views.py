from __future__ import absolute_import, print_function, unicode_literals

from flask import Blueprint, jsonify, request, current_app


from flask import Blueprint

blueprint = Blueprint(
    'publishing_search_ui',
    __name__,
    template_folder='templates',
    static_folder='static'
)