"""Record serialization."""

from __future__ import absolute_import, print_function, unicode_literals

import json
import arrow
import pycountry 

from marshmallow import Schema, fields, post_dump


class TitleSchema(Schema):
    """Title schema."""

    title = fields.Str()

class DataCiteSchema(Schema):
    """Base class for schemas."""

    identifier = fields.Method('get_identifier', attribute='metadata.doi')
    creators = fields.Method('get_creators')
    titles = fields.List(fields.Nested(TitleSchema), attribute='metadata.titles')
    publisher = fields.Constant('CERN')
    publicationYear = fields.Function(
        lambda o: str(arrow.get(o['metadata']['publication_date'], 'YYYY-MM-DD').year))
    subjects = fields.Method('get_subjects')
    language = fields.Method('get_language')
    resourceType = fields.Method('get_resourcetype')

    # descriptions = fields.Method('get_descriptions')

    def get_identifier(self, obj):
        """Get record main identifier."""
        doi = obj['metadata'].get('doi', '')
        return {
                'identifier': doi,
                'identifierType': 'DOI'
            }

    def get_creators(self, obj):
        """Get creators based on authors or collaboration field."""
        if 'authors' in obj:
            authors = obj.get('authors')
        elif 'editors' in obj:
            authors = obj.get('editors')
        else:
            authors = ['CERN']

        return [{'creatorName': x} for x in authors]

    def get_subjects(self, obj):
        """Get subjects."""
        items = []
        for s in obj['metadata'].get('keywords', []):
            items.append({'subject': s})

        return items
    
    def get_language(self, obj):
        """Export language to the Alpha-2 code (if available)."""
        for s in obj['metadata'].get('languages', []):
            l = pycountry.languages.get(name=s)
            return (l.alpha_2)
    
    def get_descriptions(self, obj):
        """."""
        items = []
        desc = obj['metadata']['descriptions']
        if desc:
            items.append({
                'description': desc,
                'descriptionType': 'Abstract'

            })
        return items
    
    def get_resourcetype(self, obj):
        """Get resource type based on type field."""
        if 'type' in obj:
            resource_type = obj['type']
        else:
            resource_type = 'Other'

        return {'resourceTypeGeneral': resource_type}