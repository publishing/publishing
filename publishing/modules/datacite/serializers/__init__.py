"""Record serialization."""

from .schemas.datacite import DataCiteSchema
from invenio_records_rest.serializers.datacite import DataCite31Serializer, \
    DataCite40Serializer

#: DataCite serializer
datacite_v40 = DataCite40Serializer(DataCiteSchema, replace_refs=True)
