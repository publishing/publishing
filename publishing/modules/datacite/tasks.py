from __future__ import absolute_import, print_function, division

from invenio_db import db
from flask import current_app
from celery import shared_task
from datetime import datetime

from invenio_pidstore.providers.datacite import DataCiteProvider
from invenio_pidstore.models import PIDStatus
from publishing.modules.datacite.serializers import datacite_v40
from publishing.modules.datacite.clients import get_datacite_client
from publishing.modules.records.api import PublishingRecord


@shared_task(ignore_result=True, max_retries=1, default_retry_delay=10 * 60,
             rate_limit='100/m')
def datacite_register(type, pid_value, record_uuid):
    """Mint DOI with DataCite.
    :param pid_value: Value of record PID.
    :type pid_value: str
    :param record_uuid: Record Metadata UUID.
    :type record_uuid: str 
    """
    try:
        record = PublishingRecord.get_record(record_uuid)

        client = get_datacite_client(record.get('search_journal_id'))

        dcp = DataCiteProvider.get(record['doi'], client=client)
        doc = datacite_v40.serialize(dcp.pid, record)

        url = current_app.config['PUBLISHING_RECORDS_UI_LINKS_FORMAT'].format(type=type,
            pid_value=pid_value)
        if dcp.pid.status == PIDStatus.REGISTERED:
            dcp.update(url, doc)
        else:
            dcp.register(url, doc)

        db.session.commit()
    except Exception as exc:
        datacite_register.retry(exc=exc)


@shared_task(ignore_result=True, max_retries=6, default_retry_delay=10 * 60,
             rate_limit='100/m')
def datacite_inactivate(doi, record):
    """Mint the DOI with DataCite.
    :param pid_value: Value of record PID.
    :type pid_value: str
    """
    try:
        client = get_datacite_client(record.get('search_journal_id'))

        dcp = DataCiteProvider.get(doi, client=client)
        dcp.delete()
        db.session.commit()
    except Exception as exc:
        datacite_inactivate.retry(exc=exc)


@shared_task(ignore_result=True, max_retries=6, default_retry_delay=10 * 60,
             rate_limit='1000/h')
def update_datacite_metadata(doi, type, object_uuid, pid_value):
    """Update DataCite metadata of a single PersistentIdentifier.
    :param doi: Value of doi PID.
    :type doi: str
    :param object_uuid: Record Metadata UUID.
    :type object_uuid: str
    """

    record = PublishingRecord.get_record(object_uuid)
    client = get_datacite_client(record.get('search_journal_id'))
    
    try:
        dcp = DataCiteProvider.get(doi, client=client)
        if dcp.pid.status != PIDStatus.REGISTERED:
            dcp.pid.sync_status(PIDStatus.REGISTERED)

        doc = datacite_v40.serialize(dcp.pid, record)

        url = current_app.config['PUBLISHING_RECORDS_UI_LINKS_FORMAT'].format(type=type,
                pid_value=pid_value)

        result = dcp.update(url, doc)
        if result is True:
            dcp.pid.updated = datetime.utcnow()
            db.session.commit()
    except Exception as exc:
        update_datacite_metadata.retry(exc=exc)
