""" Datacite clients per Journal """

from __future__ import absolute_import, print_function, division

from flask import current_app
from datacite import DataCiteMDSClient
from invenio_pidstore.providers.datacite import DataCiteProvider


def get_datacite_client(journal):
    """Create Datacite MDSClient based on journal abbreviation"""

    api = current_app.config.get('DATACITE_CLIENT_API').get(journal)
    client = None
    prefix = api.get('prefix') or \
        current_app.config.get('PIDSTORE_DATACITE_DOI_PREFIX')

    # Use default client
    if not api:
        return client

    client = DataCiteMDSClient(
        username=api.get('username'),
        password=api.get('password'),
        prefix=prefix,
        test_mode=current_app.config.get(
                    'PIDSTORE_DATACITE_TESTMODE', False),
        url=current_app.config.get('PIDSTORE_DATACITE_URL')
    )

    return client