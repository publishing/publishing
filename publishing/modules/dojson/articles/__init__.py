""" Dojson model and rules for articles """

from __future__ import absolute_import, print_function

from . import rules
from .model import articles