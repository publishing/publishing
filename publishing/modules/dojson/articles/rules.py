""" Dojson rules for articles """

from __future__ import absolute_import, division, print_function

from .model import articles

@articles.over("ojs_article" , '^85640')
def ojs_article(self, key, value):
    return value.get("u")
