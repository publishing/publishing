""" Dojson model definition for articles """

from __future__ import absolute_import, division, print_function

from ..model import FilterOverdo

filters= [
    # add_schema("http://localhost:5000/schemas/articles/articles.json"),
]

articles = FilterOverdo(filters=filters)