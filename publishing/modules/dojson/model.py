from __future__ import absolute_import, division, print_function

from functools import wraps

from dojson import Overdo
from dojson.errors import IgnoreKey


class FilterOverdo(Overdo):

    def __init__(self, filters=None, *args, **kwargs):
        super(FilterOverdo,self).__init__(*args, **kwargs)
        self.filters = filters or []

    def do(self, blob, **kwargs):
        result = super(FilterOverdo,self).do(blob, **kwargs)

        for filter_ in self.filters:
            result = filter_(result, blob)

        return result