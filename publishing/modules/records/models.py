from __future__ import absolute_import, print_function

import six
import datetime

from invenio_db import db
from sqlalchemy import func


class APIConfig(db.Model):
    """Represents a Configuration need to update record from given API."""

    __tablename__ = 'api_config'

    id = db.Column(db.Integer, primary_key=True)
    baseurl = db.Column(db.String(255), nullable=False, server_default='')
    comment = db.Column(db.Text, nullable=True)
    name = db.Column(db.String(255), nullable=False)
    lastrun = db.Column(db.DateTime, default=datetime.datetime(
        year=1900, month=1, day=1
    ), nullable=True)

    def save(self): 
        """Save object to persistent storage."""
        with db.session.begin_nested():
            db.session.merge(self)

    def update_lastrun(self, new_date=None):
        """Update the 'lastrun' attribute of object to now."""
        self.lastrun = new_date or datetime.datetime.now()


__all__ = (
    'PersistentIdentifier',
    'PIDStatus',
    'Redirect',
    'APIConfig'
)