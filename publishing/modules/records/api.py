"""Records Api """

from __future__ import absolute_import


from os.path import splitext

from invenio_db import db
from invenio_files_rest.models import Bucket
from invenio_records_files.models import RecordsBuckets
from invenio_records_files.api import FileObject, FilesIterator, Record, \
    _writable


class PublishingFileObject(FileObject):
    """Publishing file object."""

    def dumps(self):
        """Create a dump."""
        super(PublishingFileObject, self).dumps()
        self.data.update({
            # Remove dot from extension.
            'type': splitext(self.data['key'])[1][1:].lower(),
        })
        return self.data


class PublishingRecord(Record):
    """Publishing Record."""

    file_cls = PublishingFileObject

    @classmethod
    def create(cls, data, id_=None):
        """Create a record.
        Adds bucket creation immediately on record creation.
        """
        bucket = Bucket.create()
        record = super(PublishingRecord, cls).create(data, id_=id_)

        RecordsBuckets.create(record=record.model, bucket=bucket)

        return record
