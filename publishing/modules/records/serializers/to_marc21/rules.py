"""MARC21 rules."""

from __future__ import absolute_import, print_function, unicode_literals

from dojson import utils
from dojson.contrib.to_marc21 import to_marc21
from ..schemas.common import get_publishing_object


@to_marc21.over('980', '^(resource_type)$')
@utils.for_each_value
@utils.filter_values
def reverse_resource_type(dummy_self, key, value):
    """Reverse - Resource Type."""
    if key == 'resource_type':
        return {
            'a': value.get('type'),
            'b': value.get('subtype'),
            '$ind1': '_',
            '$ind2': '_',
        }


@to_marc21.over('490', '^(journal)$')
@utils.filter_values
def reverse_journal(dummy_self, key, value):
    """Journal."""
    if key == 'journal':
        journal_title = ''
        journal = get_publishing_object('jrn_id',value.get('search_journal_id'),'titles')
        if journal:
            journal_title = journal.get('title')
        return {
            'n': value.get('titles').get('title'),
            'v': str(value.get('volume')),
            'p': journal_title,
            'y': str(value.get('year')),
            '$ind1': '_',
            '$ind2': '_',
        }
    return


@to_marc21.over('856', '^_files$')
@utils.reverse_for_each_value
@utils.filter_values
def reverse_files(dummy_self, key, value):
    """Reverse - Files."""
    return {
        's': str(value['size']),
        'u': value['uri'],
        'z': value['checksum'],
        '$ind1': '4',
        '$ind2': '_',
    }


@to_marc21.over('041', '^language$')
@utils.filter_values
def reverse_language(dummy_self, dummy_key, value):
    """Reverse - language code."""
    return {
        'a': value,
        '$ind1': '_',
        '$ind2': '_',
    }