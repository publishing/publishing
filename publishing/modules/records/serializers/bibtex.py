"""BibTeX serializer for records."""

from __future__ import absolute_import, print_function, unicode_literals

import textwrap

import six
from dateutil.parser import parse as iso2dt
from flask import current_app
from slugify import slugify
from publishing.modules.records.api import PublishingRecord
from .schemas.common import get_publishing_object


class BibTeXSerializer(object):
    """BibTeX serializer for records."""

    def serialize(self, pid, record, links_factory=None):
        """Serialize a single record and persistent identifier.

        :param pid: Persistent identifier instance.
        :param record: Record instance.
        :param links_factory: Factory function for record links.
        """
        return Bibtex(record=record).format()

    def serialize_search(self, pid_fetcher, search_result, links=None,
                         item_links_factory=None):
        """Serialize a search result.

        :param pid_fetcher: Persistent identifier fetcher.
        :param search_result: Elasticsearch search result.
        :param links: Dictionary of links to add to response.
        """
        records = []
        for hit in search_result['hits']['hits']:
            records.append(Bibtex(record=hit['_source']).format())

        return "\n".join(records)


class MissingRequiredFieldError(Exception):
    """Base class for exceptions in this module.

    The exception should be raised when the specific,
    required field doesn't exist in the record.
    """

    def _init_(self, field):
        self.field = field

    def _str_(self):
        return "Missing field: " + self.field


class Bibtex(object):
    """BibTeX formatter."""

    def __init__(self, record):
        """Initialize BibTEX formatter with the specific record."""
        self.record = record

    def format(self):
        """Return BibTeX export for single record."""
        formats = {
            'article': self._format_article,
        }
        t = self._get_entry_type()
        if t in formats:
            return formats[t]()
        else:
            return formats['default']()


    def _format_entry(self, name, req, opt, ign):
        out = "@" + name + "{"
        out += self._get_citation_key() + ",\n"
        out += self._clean_input(self._fetch_fields(req, opt, ign))
        out += "}"
        return out

    def _clean_input(self,input):
        unsupported_char = ['&']
        chars = list(input)
        for index, char in enumerate(chars):
            if char in unsupported_char:
                chars[index] = "\\" + chars[index]
        return ''.join(chars)

    def _format_article(self):
        """Format article entry type.

        An article from a journal or magazine.
        """
        name = "article"
        req_fields = ['author', 'title', 'journal', 'year']
        opt_fields = ['volume', 'number', 'pages', 'month','keywords']
        ign_fields = ['doi', 'url']
        return self._format_entry(name, req_fields,
                                  opt_fields, ign_fields)


    def _format_proceedings(self):
        """Format article entry type.

        The proceedings of a conference.
        """
        name = "proceedings"
        req_fields = ['title', 'year']
        opt_fields = ['publisher', 'address', 'month']
        ign_fields = ['doi', 'url']
        return self._format_entry(name, req_fields,
                                  opt_fields, ign_fields)


    def _fetch_fields(self, req_fields, opt_fields=None, ign_fields=None):
        opt_fields = opt_fields or []
        ign_fields = ign_fields or []
        fields = {
            'author': self._get_author,
            'journal': self._get_journal,
            'month': self._get_month,
            'number': self._get_number,
            'pages': self._get_pages,
            'publisher': self._get_publisher,
            'title': self._get_title,
            'url': self._get_url,
            'volume': self._get_volume,
            'year': self._get_year,
            'doi': self._get_doi,
            'keywords':self._get_keywords
        }
        out = ""
        for field in req_fields:
            value = fields[field]()
            if value:
                out += self._format_output_row(field, value)
            else:
                raise MissingRequiredFieldError(field)
        for field in opt_fields:
            value = fields[field]()
            if value:
                out += self._format_output_row(field, value)
        for field in ign_fields:
            value = fields[field]()
            if value:
                out += self._format_output_row(field, value)
        return out

    def _format_output_row(self, field, value):
        out = ""
        if isinstance(value, six.string_types):
            value = value.strip()
        if field == "author":
            if len(value) == 1:
                out += u"  {0:<12} = {{{1}}},\n".format(field, value[0])
            else:
                out += u"  {0:<12} = {{{1} and\n".format(field, value[0])
            if len(value) > 1:
                for line in value[1:-1]:
                    out += u" {0:<16} {1:<} and\n".format("", line)
                out += u" {0:<16} {1:<}}},\n".format("", value[-1])
        elif field == "keywords":
            if len(value) == 1:
                out += u"  {0:<12} = {{{1}}},\n".format(field, value[0])
            else:
                out += u"  {0:<12} = {{{1};\n".format(field, value[0])
            if len(value) > 1:
                for line in value[1:-1]:
                    out += u" {0:<16} {1:<};\n".format("", line)
                out += u" {0:<16} {1:<}}},\n".format("", value[-1])
        elif field == "month":
            out += u"  {0:<12} = {1},\n".format(field, value)
        elif field == "url":
            out += u"  {0:<12} = {{{1}}}\n".format(field, value)
        else:
            if(self._is_number(value)):
                out += u"  {0:<12} = {1},\n".format(field, value)
            else:
                out += u"  {0:<12} = {{{1}}},\n".format(field, value)
        return out

    def _is_number(self, s):
        try:
            int(s)
            return True
        except (ValueError, TypeError):
            return False

    def _get_entry_type(self):
        """Return entry type."""
        if 'resource_type' in self.record:
            if 'type' in self.record['resource_type']:
                return self.record['resource_type']['type']
        return 'article'

    def _get_citation_key(self):
        """Return citation key."""
        if "art_id" in self.record:
            authors = self.record.get("authors", None)
            if authors:
                first_author = authors[0]
                name = first_author.get(
                    "fullname",
                    first_author.get("givenname")
                )
                pubdate = self.record.get('publication_date', None)
                if pubdate:
                    year = "{}_{}".format(iso2dt(pubdate).year,
                                          self.record['art_id'])
                else:
                    year = self.record['art_id']

                return "{0}_{1}".format(slugify(name, separator="_",
                                                max_length=40),
                                        year)
            else:
                return six.text_type(self.record['art_id'])
        else:
            raise MissingRequiredFieldError("citation key")

    def _get_doi(self):
        """Return doi."""
        if "doi" in self.record:
            return self.record['doi']
        else:
            return None

    def _get_author(self):
        """Return list of name(s) of the author(s)."""
        result = []
        if "authors" in self.record:
            for author in self.record['authors']:
                result.append(author["fullname"])
            return result
        else:
            return result

    def _get_title(self):
        """Return work's title."""
        if "titles" in self.record:
            return self.record['titles']['title'].strip()
        else:
            return ""

    def _get_month(self):
        """Return the month in which the work was published."""
        months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun',
                  'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
        if "publication_date" in self.record:
            return months[iso2dt(self.record["publication_date"]).month - 1]
        else:
            return ""

    def _get_year(self):
        """Return the year of publication."""
        if "publication_date" in self.record:
            return six.text_type(iso2dt(self.record["publication_date"]).year)
        else:
            return ""


    def _get_journal(self):
        """Return the journal or magazine the work was published in."""
        journal = get_publishing_object('jrn_id',
                            self.record["search_journal_id"], 'titles')
        if journal:
            return journal.get('title', '')
                

    def _get_number(self):
        """Return the (issue) number of a issue."""
        issue = get_publishing_object('iss_id',
                        self.record["search_issue_id"],'issue')
        if issue:
            return issue
        else:
            return "0"


    def _get_pages(self):
        """Return page numbers, separated by commas or double-hyphens."""
        if "pages" in self.record:
            return str(self.record["pages"])
        else:
            return ""

    def _get_publisher(self):
        """Return the publisher's name."""
        return current_app.config.get("THEME_SITENAME", "")


    def _get_url(self):
        """Return the WWW address."""
        return "https://doi.org/%s" % self.record['doi'] \
            if "doi" in self.record else ""


    def _get_keywords(self):
        """Return keywords."""
        result = []
        if "keywords" in self.record:
            for keyword in self.record['keywords']:
                result.append(keyword)
            return result
        else:
            return result


    def _get_volume(self):
        """Return the volume of a journal or multi-volume book."""
        issue = get_publishing_object('iss_id',
                        self.record["search_issue_id"],'volume')
        if issue:
            return str(issue)
        else:
            return ""