from invenio_records_rest.serializers.dc import DublinCoreSerializer
from invenio_records_rest.serializers.citeproc import CiteprocSerializer
from invenio_records_rest.serializers.json import JSONSerializer
from invenio_marc21.serializers.marcxml import MARCXMLSerializer
from invenio_records_rest.serializers.response import record_responsify, \
    search_responsify
from dojson.contrib.to_marc21 import to_marc21


from .json import PublishingJSONSerializer
from .bibtex import BibTeXSerializer
from .schemas.csl import RecordSchemaCSLJSON
from .schemas.json import  RecordSchemaV1
from .schemas.dc import DublinCoreV1
from .schemas.marc21 import RecordSchemaMARC21



# Serializers
# ===========
#: Publishing JSON serializer version 1.0.0
json_v1 = PublishingJSONSerializer(RecordSchemaV1, replace_refs=True)
#: CSL-JSON serializer
csl_v1 = PublishingJSONSerializer(RecordSchemaCSLJSON, replace_refs=True)
#: CSL Citation Formatter serializer
citeproc_v1 = CiteprocSerializer(csl_v1)
#: BibTeX serializer version 1.0.0
bibtex_v1 = BibTeXSerializer()
#: Dublin Core serializer
dc_v1 = DublinCoreSerializer(DublinCoreV1, replace_refs=True)
#: MARCXML serializer version 1.0.0
marcxml_v1 = MARCXMLSerializer(
    to_marc21, schema_class=RecordSchemaMARC21, replace_refs=True)



# Records-REST serializers
# ========================
#: JSON record serializer for individual records.
json_v1_response = record_responsify(json_v1, 'application/json')

#: CSL-JSON record serializer for individual records.
csl_v1_response = record_responsify(
    csl_v1, 'application/vnd.citationstyles.csl+json')
#: CSL Citation Formatter serializer for individual records.
citeproc_v1_response = record_responsify(citeproc_v1, 'text/x-bibliography')
#: BibTeX record serializer for individual records.
bibtex_v1_response = record_responsify(bibtex_v1, 'application/x-bibtex')
#: DublinCore record serializer for individual records.
dc_v1_response = record_responsify(dc_v1, 'application/x-dc+xml')
#: MARCXML record serializer for individual records.
marcxml_v1_response = record_responsify(marcxml_v1, 'application/marcxml+xml')


#: JSON record serializer for search results.
json_v1_search = search_responsify(json_v1, 'application/json')
#: BibTeX serializer for search records.
bibtex_v1_search = search_responsify(bibtex_v1, 'application/x-bibtex')
#: DublinCore record serializer for search records.
dc_v1_search = search_responsify(dc_v1, 'application/x-dc+xml')
