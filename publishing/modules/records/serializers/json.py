"""Marshmallow based JSON serializer for records."""

from __future__ import absolute_import, print_function

from flask import json, request
from invenio_records_rest.serializers.json import JSONSerializer
from invenio_pidstore.models import PersistentIdentifier
from invenio_pidstore.errors import PIDDoesNotExistError
from publishing.modules.records.api import PublishingRecord





class PublishingJSONSerializer(JSONSerializer):
    """Mixin serializing records as JSON."""


    def serialize_search(self, pid_fetcher, search_result, links=None,
                         item_links_factory=None, **kwargs):
        """Serialize a search result.
        :param pid_fetcher: Persistent identifier fetcher.
        :param search_result: Elasticsearch search result.
        :param links: Dictionary of links to add to response.
        """
        aggregations = search_result.get('aggregations', dict())
        
        # Populate aggregations with Journal name for the ui
        if aggregations:
            try:
                for bucket in aggregations.get('Journal').get('buckets'):
                    try:
                        obj = PersistentIdentifier.get('jrn_id', bucket['key'])
                        record = PublishingRecord.get_record(obj.object_uuid)
                        if record and record.get('jrn_id'):
                            bucket['name'] = record.get('titles').get('title')
                        else:
                            bucket['status'] = 'deleted'
                    except PIDDoesNotExistError:
                        pass
            except AttributeError:
                pass

        return json.dumps(dict(
            hits=dict(
                hits=[self.transform_search_hit(
                    pid_fetcher(hit['_id'], hit['_source']),
                    hit,
                    links_factory=item_links_factory,
                    **kwargs
                ) for hit in search_result['hits']['hits']],
                total=search_result['hits']['total']['value'],
            ),
            links=links or {},
            aggregations=aggregations,
        ), **self._format_args())





