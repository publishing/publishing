from __future__ import absolute_import, print_function, unicode_literals

import arrow
import idutils
import jsonref
import pycountry
from flask import current_app, has_request_context
from flask_babelex import lazy_gettext as _
from invenio_iiif.previewer import previewable_extensions as thumbnail_exts
from invenio_iiif.utils import ui_iiif_image_url
from invenio_pidstore.errors import PIDDoesNotExistError
from invenio_pidstore.models import PersistentIdentifier
from invenio_records.api import Record
from marshmallow import Schema, ValidationError, fields, missing, post_dump, \
    post_load, pre_dump, pre_load, validate, validates, validates_schema
from six.moves.urllib.parse import quote
from werkzeug.routing import BuildError
from publishing.modules.records.api import PublishingRecord




def clean_empty(data, keys):
    """Clean empty values."""
    for k in keys:
        if k in data and not data[k]:
            del data[k]
    return data


URLS = {
    'bucket': '{base}/files/{bucket}',
    'object': '{base}/files/{bucket}/{key}',
    'journal_html': '/journals/{id}',
    'issue_html': '/issues/{id}',
    'article_html': '/articles/{id}',
    'record_file': '{base}/record/{id}/files/{filename}',
    'thumbnail': '{base}{path}',
}


def get_publishing_object(pid_type, pid_value, key): 
    """Retrieve Publishing record object values"""
    try:
        rec_pid = PersistentIdentifier.get(pid_type,
                                    pid_value)
        rec = PublishingRecord.get_record(rec_pid.object_uuid)
        if rec.get(pid_type):
            if not key:
                return rec
            pub_obj = rec.get(key)
            return pub_obj
        return ''
    except (PIDDoesNotExistError, KeyError):
        return None


def link_for(base, tpl, **kwargs):
    """Create a link using specific template."""
    tpl = URLS.get(tpl)
    for k in ['key', ]:
        if k in kwargs:
            kwargs[k] = quote(kwargs[k].encode('utf8'))
    return tpl.format(base=base, **kwargs)


def api_link_for(tpl, **kwargs):
    """Create an API link using specific template."""
    is_api_app = 'invenio-deposit-rest' in current_app.extensions

    base = '{}/api'
    if current_app.testing and is_api_app:
        base = '{}'

    return link_for(
        base.format(current_app.config['THEME_SITEURL']), tpl, **kwargs)

def ui_link_for(tpl, **kwargs):
    """Create an UI link using specific template."""
    return link_for(current_app.config['THEME_SITEURL'], tpl, **kwargs)


class CommonRecordSchemaV1(Schema):
    """Common record schema."""

    id = fields.Integer(attribute='pid.pid_value', dump_only=True)
    links = fields.Method('dump_links', dump_only=True)
    created = fields.Str(dump_only=True)


    def dump_links(self, obj):
        """Dump links."""
        links = obj.get('links', {})
        if current_app:
            links.update(self._dump_common_links(obj))

        try:
            m = obj.get('metadata', {})
            links.update(self._dump_record_links(obj))
        except BuildError:
            pass
        return links

    def _thumbnail_url(self, fileobj, thumbnail_size):
        """Create the thumbnail URL for an image."""
        return link_for(
            current_app.config.get('THEME_SITEURL'),
            'thumbnail',
            path=ui_iiif_image_url(
                fileobj,
                size='{},'.format(thumbnail_size),
                image_format='png' if fileobj['type'] == 'png' else 'jpg',
            )
        )

    def _dump_common_links(self, obj):
        """Dump common links for records."""
        links = {}
        m = obj.get('metadata', {})

        doi = m.get('doi')
        if doi:
            links['doi'] = idutils.to_url(doi, 'doi', 'https')

        files = m.get('_files', [])
        for f in files:
            if f.get('label') == "Cover":
                try:
                    links['thumb250'] = self._thumbnail_url(f, 80)
                except RuntimeError:
                    pass
                break

        return links

    def _dump_record_links(self, obj):
        """Dump record-only links."""
        links = {}
        m = obj.get('metadata')

        jrn_id = m.get('jrn_id')
        iss_id = m.get('iss_id')
        art_id = m.get('art_id')

        if jrn_id:
            links['html'] = ui_link_for('journal_html', id=jrn_id)
        if iss_id:
            links['html'] = ui_link_for('issue_html', id=iss_id)
        if art_id:
            links['html'] = ui_link_for('article_html', id=art_id)

        # Generate relation links
        return links


    @post_load(pass_many=False)
    def remove_envelope(self, data):
        """Post process data."""
        # Remove envelope
        if 'metadata' in data:
            data = data['metadata']

        return data
