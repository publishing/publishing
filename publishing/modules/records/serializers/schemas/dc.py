"""Record serialization."""

from __future__ import absolute_import, print_function

import lxml.html
from marshmallow import Schema, fields, missing
from publishing.modules.records.api import PublishingRecord

from .common import ui_link_for, get_publishing_object


class DublinCoreV1(Schema):
    """Schema for records v1 in JSON."""

    identifiers = fields.Method('get_identifiers')
    titles = fields.Function(lambda o: [o['metadata'].get('title', u'')])
    creators = fields.Method('get_authors')
    dates = fields.Method('get_dates')
    subjects = fields.Method('get_subjects')
    descriptions = fields.Method('get_descriptions')
    publishers = fields.Method('get_publishers')
    types = fields.Method('get_types')
    sources = fields.Method('get_sources')

    def get_identifiers(self, obj):
        """Get identifiers."""
        items = []
        items.append(u'https://publishing.cern.ch/articles/{0}'.format(
            obj['metadata']['art_id']))
        items.append(obj['metadata'].get('doi', u''))
        return items

    def get_authors(self, obj):
        """Get creators."""
        return [c['fullname'] for c in obj['metadata'].get('authors', [])]


    def get_dates(self, obj):
        """Get dates."""
        dates = [obj['metadata']['publication_date']]

        return dates

    def get_descriptions(self, obj):
        """Get descriptions."""
        descriptions = []
        desc = obj['metadata'].get('descriptions')
        if desc:
            if desc.get('description', '').strip():
                descriptions.append(
                    lxml.html.document_fromstring(desc['description'])
                    .text_content().replace(u"\xa0", u" "))
        return descriptions

    def get_subjects(self, obj):
        """Get subjects."""
        metadata = obj['metadata']
        subjects = []
        subjects.extend(metadata.get('keywords', []))

        return subjects

    def get_publishers(self, obj):
        """Get publishers."""
        return ['CERN']


    def get_types(self, obj):
        """Get types."""
        return ['article']

    def get_sources(self, obj):
        """Get sources."""
        items = []

        # Journal
        search_journal_id = obj['metadata'].get('search_journal_id')
        if search_journal_id is not None:
            journal = get_publishing_object('jrn_id',
                       search_journal_id,'titles')
            
        # Issue
        search_issue_id = obj['metadata'].get('search_issue_id')
        if search_issue_id is not None:
            vol = get_publishing_object('iss_id',
                       search_issue_id,'volume')
            issue = get_publishing_object('iss_id',
                       search_issue_id,'issue')
            if vol and issue:
                vol = u'{0}({1})'.format(vol, issue)
            if vol is None:
                vol = issue

            y = get_publishing_object('iss_id',
                       search_issue_id,'year')

            parts = [
                journal.get('title'),
                str(vol),
                u'({0})'.format(y) if y else None,
            ]
            items.append(u' '.join([x for x in parts if x]))

        return items