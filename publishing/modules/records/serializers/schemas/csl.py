"""CERN Publishing CSL-JSON schema."""

from __future__ import absolute_import, print_function

import re

from invenio_formatter.filters.datetime import from_isodate
from marshmallow import Schema, fields, missing
from publishing.modules.records.api import PublishingRecord
from .common import get_publishing_object



class AuthorSchema(Schema):
    """Schema for an author."""

    family = fields.Method('get_family_name')
    given = fields.Method('get_given_names')

    def get_family_name(self, obj):
        """Get family name."""
        if {'familyname', 'givenname'} <= set(obj):
            return obj.get('familyname')
        return missing

    def get_given_names(self, obj):
        """Get given names."""
        if {'familyname', 'givenname'} <= set(obj):
            return obj.get('givenname')
        return missing


class RecordSchemaCSLJSON(Schema):
    """Schema for records in CSL-JSON."""

    id = fields.Str(attribute='pid.pid_value')
    type = fields.Method('get_type')
    title = fields.Str(attribute='metadata.titles.title')
    abstract = fields.Str(attribute='metadata.descriptions.description')
    author = fields.List(fields.Nested(AuthorSchema),
                         attribute='metadata.authors')
    issued = fields.Method('get_issue_date')
    language = fields.List(fields.Str(), attribute='metadata.languages')

    DOI = fields.Str(attribute='metadata.doi')

    container_title = fields.Method('get_journal_title')
    page = fields.Str(attribute='metadata.pages')
    volume = fields.Method('get_issue_volume')
    issue = fields.Method('get_issue_title')
    publisher = fields.Method('get_publisher')

    def get_issue(self, obj, key):
        """Get issue."""
        m = obj['metadata']
        issue = get_publishing_object('iss_id',
                        m.get('search_issue_id'),key)
        return issue or missing


    def get_journal_title(self, obj):
        """Get journal title."""
        m = obj['metadata']
        journal = get_publishing_object('jrn_id',
                        m.get('search_journal_id'),'titles')
        return journal.get('title') or missing


    def get_issue_volume(self, obj):
        """Get issue volume"""
        return self.get_issue(obj, 'volume')
    

    def get_issue_title(self, obj):
        """Get issue volume"""
        return self.get_issue(obj, 'titles').get('title')
    

    def get_publisher(self, obj):
        """Get publisher."""
        m = obj['metadata']
        publisher = m.get('publisher')
        if publisher:
            return publisher
        else:
            return 'CERN'

    def get_type(self, obj):
        """Get record CSL type."""
        return 'article'

    def get_issn(self, obj):
        """Get the record's ISSN."""
        for id in obj['metadata'].get('alternate_identifiers', []):
            if id['scheme'] == 'issn':
                return id['identifier']
        return missing

    def get_issue_date(self, obj):
        """Get a date in list format."""
        d = from_isodate(obj['metadata'].get('publication_date'))
        return {'date-parts': [[d.year, d.month, d.day]]} if d else missing