"""MARCXML translation index."""

from __future__ import absolute_import, print_function, unicode_literals

import urllib.parse
from dateutil.parser import parse
from flask import current_app
from marshmallow import Schema, fields, missing, post_dump
from publishing.modules.records.api import PublishingRecord
from .common import get_publishing_object


class RecordSchemaMARC21(Schema):
    """Schema for records in MARC."""

    control_number = fields.Function(
        lambda o: str(o['metadata'].get('art_id')))

    date_and_time_of_latest_transaction = fields.Function(
        lambda obj: parse(obj['updated']).strftime("%Y%m%d%H%M%S.0"))


    index_term_uncontrolled = fields.Function(
        lambda o: [
            dict(uncontrolled_term=kw)
            for kw in o['metadata'].get('keywords', [])
        ]
    )

    subject_added_entry_topical_term = fields.Method(
        'get_subject_added_entry_topical_term')

    terms_governing_use_and_reproduction_note = fields.Function(
        lambda o: dict(
            uniform_resource_identifier=o[
                'metadata'].get('license', {}).get('license_uri'),
            terms_governing_use_and_reproduction=o[
                'metadata'].get('license', {}).get('license')
        ))

    title_statement = fields.Function(
        lambda o: dict(title=o['metadata'].get('titles').get('title')))


    publication_distribution_imprint = fields.Method(
        'get_publication_distribution_imprint')


    other_standard_identifier = fields.Method('get_other_standard_identifier')

    main_entry_personal_name = fields.Method('get_main_entry_personal_name')

    added_entry_personal_name = fields.Method('get_added_entry_personal_name')

    summary = fields.Function(
        lambda o: dict(summary=o['metadata'].get('descriptions').get('description')))

    # Custom
    # ======

    resource_type = fields.Raw(attribute='record_type')

    journal = fields.Method('get_journal')

    _files = fields.Method('get_files')


    def get_files(self, o):
        """Get the files provided the record is open access."""

        res = []
        for f in o['metadata'].get('_files', []):
            key = urllib.parse.quote(f['key'])
            res.append(dict(
                uri=u'{0}/articles/{1}/preview/{2}'.format(
                    current_app.config.get('THEME_SITEURL'),
                    o['metadata'].get('art_id', ''), key ),
                size=f['size'],
                checksum=f['checksum'],
                type=f['type'],
            ))
        return res or missing
    

    def get_journal(self, o):
        """Get the files provided the record is open access."""
        m = o['metadata']
        journal = get_publishing_object('iss_id',
                        m.get('search_issue_id'),'')
        
        return journal or missing


    def get_publication_distribution_imprint(self, o):
        """Get publication date and imprint."""
        res = []

        pubdate = o['metadata'].get('publication_date')
        if pubdate:
            res.append(dict(date_of_publication_distribution=pubdate))

        return res or missing

    def get_subject_added_entry_topical_term(self, o):
        """Get licenses and subjects."""
        res = []

        license = o['metadata'].get('license', {}).get('id')
        if license:
            res.append(dict(
                topical_term_or_geographic_name_entry_element='cc-by',
                source_of_heading_or_term='opendefinition.org',
                level_of_subject='Primary',
                thesaurus='Source specified in subfield $2',
            ))

        return res or missing

    def get_other_standard_identifier(self, o):
        """Get other standard identifiers."""
        res = []

        def stdid(pid, scheme, q=None):
            return dict(
                standard_number_or_code=pid,
                source_of_number_or_code=scheme,
                qualifying_information=q,
            )
        m = o['metadata']
        if m.get('doi'):
            res.append(stdid(m['doi'], 'doi'))

        return res or missing

    def _get_personal_name(self, v, relator_code=None):
        ids = []
        for scheme in ['gnd', 'orcid', ]:
            if v.get(scheme):
                ids.append((scheme, v[scheme]))

        return dict(
            personal_name=v.get('fullname'),
            affiliation=v.get('affiliation'),
            authority_record_control_number_or_standard_number=[
                "({0}){1}".format(scheme, identifier)
                for (scheme, identifier) in ids
            ]        
        )

    def get_main_entry_personal_name(self, o):
        """Get main_entry_personal_name."""
        authors = o['metadata'].get('authors', [])
        if len(authors) > 0:
            v = authors[0]
            return self._get_personal_name(v)

    def get_added_entry_personal_name(self, o):
        """Get added_entry_personal_name."""
        items = []
        creators = o['metadata'].get('authors', [])
        if len(creators) > 1:
            for c in creators[1:]:
                items.append(self._get_personal_name(c))

        return items


    @post_dump(pass_many=True)
    def remove_empty_fields(self, data, many):
        """Dump + Remove empty fields."""
        _filter_empty(data)
        return data


def _is_non_empty(value):
    """Conditional for regarding a value as 'non-empty'.

    This enhances the default "bool" return value with some exceptions:
     - number 0 resolves as True - non-empty.
    """
    if isinstance(value, int):
        return True  # All integers are non-empty values
    else:
        return bool(value)


def _filter_empty(record):
    """Filter empty fields."""
    if isinstance(record, dict):
        for k in list(record.keys()):
            if _is_non_empty(record[k]):
                _filter_empty(record[k])
            if not _is_non_empty(record[k]):
                del record[k]
    elif isinstance(record, list) or isinstance(record, tuple):
        for (k, v) in list(enumerate(record)):
            if _is_non_empty(v):
                _filter_empty(record[k])
            if not _is_non_empty(v):
                del record[k]