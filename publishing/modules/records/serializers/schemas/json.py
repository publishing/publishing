from __future__ import absolute_import, print_function, unicode_literals

from flask_babelex import lazy_gettext as _
from invenio_pidstore.models import PersistentIdentifier
from marshmallow import Schema, ValidationError, fields, missing, \
    validates_schema
from werkzeug.routing import BuildError

from publishing.modules.stats.utils import get_record_stats

from . import common

class FilesSchema(Schema):
    """Files metadata schema."""

    type = fields.String()
    checksum = fields.String()
    size = fields.Integer()
    bucket = fields.String()
    key = fields.String()
    links = fields.Method('get_links')
    file_name = fields.String()
    label = fields.String()
    revision = fields.Integer()

    def get_links(self, obj):
        """Get links."""
        return {
            'self': common.api_link_for(
                'object', bucket=obj['bucket'], key=obj['key'])
        }

class RecordSchemaV1(common.CommonRecordSchemaV1):
    """Schema for records v1 in JSON."""

    files = fields.Nested(
        FilesSchema, many=True, dump_only=True, attribute='_files')
    metadata = fields.Raw()
    record_type = fields.Method('get_type')
    updated = fields.Str(dump_only=True)

    stats = fields.Method('dump_stats')

    def dump_stats(self, obj):
        """Dump the stats to a dictionary."""
        if '_stats' in obj.get('metadata', {}):
            return obj['metadata'].get('_stats', {})
        else:
            pid = self.context.get('pid')
            if isinstance(pid, PersistentIdentifier):
                return get_record_stats(pid.object_uuid, False)
            else:
                return None
    
    def get_type(self, obj):
        """Get record main identifier."""
        if 'jrn_id' in obj.get('metadata', {}):
            return 'Journal'
        elif 'iss_id' in obj.get('metadata',{}):
            return 'Issue'
        elif 'art_id' in obj.get('metadata',{}):
            return 'Article'
