"""Record modification prior to indexing."""

from __future__ import absolute_import, print_function
from flask import current_app
from invenio_pidstore.models import PersistentIdentifier

from publishing.modules.stats.utils import build_record_stats


def indexer_receiver(sender, json=None, record=None, index=None,
                     **dummy_kwargs):
    """Connect to before_record_index signal to transform record for ES."""

    pid_value = json.get('art_id') or json.get('iss_id') or \
        json.get('jrn_id')
    
    extra_params = current_app.config.get('SEARCH_MAPPINGS_EXTRA_PARAMS',[])
    if extra_params:
        json['_search'] = dict()

    for param in extra_params:
        value_from = param.get('object_path_from',())
        value_to = param.get('object_path_to',())

        if not value_from or not value_to:
            continue
        
        value = get_record_value(record, value_from)
        if value:
            value_dict = set_search_values(value_to, value, json={})
            _search = json['_search']
            for key in value_dict.keys():
                if key in _search.keys():
                     _search[key].update(value_dict[key])
                else:
                     _search[key]=value_dict[key]
    
    if pid_value:
        json['_stats'] = build_record_stats(pid_value)
    


def get_record_value(record, values):
    if values:
        a, *b = values
        if isinstance(record, dict):
            return get_record_value(record.get(a),b)
        elif record:
            return record
    else:
        return record


def set_search_values(values, value, json={}):
    if values:
        *a, b = values
        json[b] = value
        return set_search_values(a, json, json={})
    else:
        return dict(value)
