"""Persistent identifier fetcher"""

from __future__ import absolute_import, print_function

from invenio_pidstore.fetchers import FetchedPID
from .providers import JournalProvider, IssueProvider, ArticleProvider


def journal_fetcher(record_uuid, data):
    """Fetch PID from issue record."""
    return FetchedPID(
        provider=JournalProvider,
        pid_type=JournalProvider.pid_type,
        pid_value=data['jrn_id']
    )


def issue_fetcher(record_uuid, data):
    return FetchedPID(
        provider=IssueProvider,
        pid_type=IssueProvider.pid_type,
        pid_value=data['iss_id']
    )


def article_fetcher(record_uuid, data):
    """Fetch PID from issue record."""
    return FetchedPID(
        provider=ArticleProvider,
        pid_type=ArticleProvider.pid_type,
        pid_value=data['art_id']
    )


def combined_fetcher(record_uuid, data):
    if 'art_id' in data:
        return article_fetcher(record_uuid, data)
    elif 'iss_id' in data:
        return issue_fetcher(record_uuid, data)
    elif 'jrn_id' in data:
        return journal_fetcher(record_uuid, data)
