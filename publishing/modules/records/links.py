from flask import request, url_for

from invenio_records_rest.proxies import current_records_rest


def combined_links_factory(pid, record=None, **kwargs):
    """Factory for record links generation.
    :param pid: A Persistent Identifier instance.
    :returns: Dictionary containing a list of useful links for the record.
    """
    endpoint = '.{0}_item'.format(
        current_records_rest.default_endpoint_prefixes[pid.pid_type])
    links = dict(self=url_for(endpoint, pid_value=pid.pid_value,
                              _external=True))
    return links
