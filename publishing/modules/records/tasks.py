"""Celery background tasks."""

import warnings
from flask import current_app
from celery import shared_task
from requests.exceptions import RetryError
from invenio_records.models import RecordMetadata

from publishing.modules.oaiharvester.utils import get_records_api
from publishing.modules.records.utils import list_ojs_ids, delete_record
from publishing.modules.oaiharvester.process import extract_issues_from_ojs


@shared_task()
def delete_article_records():
    """Delete multiple article records."""
    # Get all record metadata that exist in invenio database
    sources = current_app.config.get('PROVENANCE_SOURCES')
    provenance = '{{"provenance":{{"source":"{0}"}}}}'.format(sources)
    recs_meta = [rec_meta.json for rec_meta in RecordMetadata.query.filter(
                                        RecordMetadata.json.op('@>')(provenance),
                                        RecordMetadata.json.op('?')('art_id')
                                        ).all()]

    articles = {}
    for rec in recs_meta:
        if 'ojs_article_id' in rec['provenance']['identifiers']:
            articles[rec['art_id']] = rec['provenance']['identifiers']['ojs_article_id']

    try:
        ojs_ids = set(get_records_api('articles'))

        for invenio_pid_value, ojs_id in articles.items():
            if ojs_id not in ojs_ids:
                delete_record('art_id', invenio_pid_value)
    except RetryError:
         warnings.warn(
            'Requests to the API failed., delete articles records failed'
        )


@shared_task()
def delete_issue_records():
    """Delete multiple issue records."""
    # Get all record metadata that exist in invenio database
    sources = current_app.config.get('PROVENANCE_SOURCES')
    provenance = '{{"provenance":{{"source":"{0}"}}}}'.format(sources)
    recs_meta = [rec_meta.json for rec_meta in RecordMetadata.query.filter(
                                    RecordMetadata.json.op('@>')(provenance),
                                    RecordMetadata.json.op('?')('iss_id')
                                    ).all()]

    issues = {}
    for rec in recs_meta:
        if 'ojs_issue_id' in rec['provenance']['identifiers']:
            issues[rec['iss_id']] = rec['provenance']['identifiers']['ojs_issue_id']

    try:
        ojs_ids = set(get_records_api('issues'))

        for invenio_pid_value, ojs_id in issues.items():
            if ojs_id not in ojs_ids:
                delete_record('iss_id', invenio_pid_value)
    except RetryError:
         warnings.warn(
            'Requests to the API failed., delete issue records failed'
        )


@shared_task()
def update_issues_from_api(from_date=None, url=None,
                            name=None, **kwargs):
    """Update multiple issue records from an API.
    :param from_date: The lower bound date for the updating.
    :param url: The The url to be used to create the endpoint.
    :param name: The name of the APIConfig to use instead of passing
                 specific parameters.
    """

    ojs_ids = list_ojs_ids(
        from_date,
        url,
        name
    )

    if ojs_ids:
        extract_issues_from_ojs(ojs_ids)