from invenio_access import Permission, any_user



def files_permission_factory(obj, action):
    """Get default files permission factory.
    """
    return Permission(any_user)
