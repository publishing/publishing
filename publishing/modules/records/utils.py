from __future__ import absolute_import, print_function, division

import requests
import datetime

from six import BytesIO
from flask import current_app
from sqlalchemy.orm.attributes import flag_modified
from elasticsearch.exceptions import NotFoundError, TransportError
from elasticsearch_dsl import Q
from requests.exceptions import RetryError

from invenio_db import db
from invenio_pidstore import current_pidstore
from invenio_indexer.api import RecordIndexer
from invenio_records_files.models import RecordsBuckets
from invenio_files_rest.models import Bucket
from invenio_pidstore.models import PersistentIdentifier
from invenio_pidstore.errors import PIDDoesNotExistError
from invenio_records_files.utils import record_file_factory
from invenio_search.api import RecordsSearch
from invenio_oaiharvester.errors import NameOrUrlMissing, InvenioOAIHarvesterConfigNotFound
from publishing.modules.records.api import PublishingRecord
from publishing.modules.oaiharvester.utils import request_record, \
    generate_invenio_file_name, requests_retry_session
from publishing.modules.datacite.tasks import datacite_inactivate


def list_ojs_ids(from_date=None, url=None, name=None):
    """Get multiple records from an API.

    :param from_date: The lower bound date for the updating.
    :param url: The The url to be used to create the endpoint.
    :param name: The name of the APIConfig to use instead of passing
                 specific parameters.
    :return: list of OJS ids.
    """
    lastrun = None

    if name:
        url, lastrun  = get_info_by_api_name(name)

    elif not url:
        raise NameOrUrlMissing(
            "Retry using the parameters -n <name> or -u <url>."
        )
    
    # By convention, when we have a url we have no lastrun, and when we use
    # the name we can either have from_date (if provided) or lastrun.
    date = from_date or lastrun
    
    try:
        response = requests_retry_session().get(url+date).json()

        lastrun_date = datetime.datetime.now()

        # Use a list to gather ojs ids.
        ojs_ids = []

        for rec in response['data']:
            ojs_ids.append(rec['id'])

        # Update lastrun?
        if from_date is None and name is not None:
            oai_source = get_updatefromapi_object(name)
            oai_source.update_lastrun(lastrun_date)
            oai_source.save()
            db.session.commit()
        return ojs_ids
    except RetryError:
        print(
            'Request to API failed.'
        )
        return None


def get_info_by_api_name(name):
    """Get basic API request data from the APIConfig model.

    :param name: name of the source (APIConfig.name)
    :return: (url, lastrun as YYYY-MM-DD HΗ:MM:SS)
    """
    obj = get_updatefromapi_object(name)
    lastrun = obj.lastrun.strftime("%Y-%m-%d %H:%M:%S")
    return obj.baseurl, lastrun


def get_updatefromapi_object(name):
    """Query and returns an APIConfig object based on its name.

    :param name: The name of the APIConfig object.
    :return: The APIConfig object.
    """
    from .models import APIConfig
    obj = APIConfig.query.filter_by(name=name).first()
    if not obj:
        raise InvenioOAIHarvesterConfigNotFound(
            'Unable to find APIConfig obj with name %s.'
            % name
        )

    return obj


def insert_record(minter, data):
    """Insert and index record to Invenio Instance.

    :param minter: Minter.
    :param data: Metadata of the record.
    :return: The PublishingRecord object.
    """

    pid_minter = current_pidstore.minters[minter]

    record = PublishingRecord.create(data)
    pid_minter(record.id, record)
    record.commit()
    RecordIndexer().bulk_index([str(record.id)])
    db.session.commit()
    return record


def create_file(file, record):
    """Create '_files' in the invenio record, using 
    files information from OJS API

    :param file: File of the record.
    :param record: A PublishingRecord object.
    """

    key = generate_invenio_file_name(file)
    link = file['galley']

    r = requests.get(link)
    record.files[key] = BytesIO(r.content)
    record['_files'] = record.files.dumps()
    fileobject = record_file_factory(None, record, key)
    
    if 'label' in file:
        fileobject.data.update({'label': file.get('label')})

    if 'revision' in file:
        fileobject.data.update({'revision': file['revision']})
    
    if 'file_name' in file:
        fileobject.data.update({'file_name': file['file_name']})
    
    if 'galley_type' in file:
        fileobject.data.update({'galley_type': file['galley_type']})


    record = record.commit()
    RecordIndexer().bulk_index([str(record.id)])
    db.session.commit()


def update_files(files, existing_record):
    """Update files for record

    :param files: Files of the record.
    :param record: A PublishingRecord object.
    """

    # Create bucket in case was deleted.
    if existing_record.files is None:
        bucket = Bucket.create()
        RecordsBuckets.create(record=existing_record.model, bucket=bucket)

    # Remove all files.
    if not files:
        for key in existing_record.files.keys:
            existing_record.files.__delitem__(key)

    if files:
        for file in files:
            invenio_file_name = generate_invenio_file_name(file)

            if invenio_file_name in existing_record.files:

                # Only articles contain revision.
                if 'revision' in file:

                    fileobject = record_file_factory(
                                        None,
                                        existing_record,
                                        invenio_file_name
                                    )

                    if fileobject.data:
                        if file['revision'] != fileobject.data['revision']:
                            create_file(file, existing_record)
                    else:
                        create_file(file, existing_record)

                else:
                    create_file(file, existing_record)
            else:
                create_file(file, existing_record)

        # Delete not existing files.
        for key in existing_record.files.keys:
            if not any(d['file_name'] == key.split('_',1)[1] and d['file_id'] == key.split('_',1)[0]
                       for d in files):
                existing_record.files.__delitem__(key)
        
    return existing_record


def create_record_metadata(type, ojs_id):
    """Create record metadata, ojs_record metadata and files metadata
    based on the OJS-API data.

    :param type: The type of record(eg. articles).
    :param ojs_id: The ojs id of record.
    :return record: The record metadata dictionary.
    :return ojs_meta: The record meta dictionary.
    :return files: The record files dictionary.
    """

    files = []
    data = request_record(type, ojs_id)
    record = data['data']['attributes']
    ojs_meta = data['meta']

    if 'galleys' in ojs_meta:
        files = data['meta']['galleys']
    
    return record, ojs_meta, files


def retrieve_record(pid_type, pid_value):
    """Function to return the invenio_record
    
    :param pid_type: The type of pid(eg. art_id).
    :param pid_value: The value of PID.
    :return: The PublishingRecord object.
    """

    obj = PersistentIdentifier.get(pid_type, pid_value)
    return PublishingRecord.get_record(obj.object_uuid)        


def update_invenio_json(record, metadata):
    """Update Json in an invenio Record
    
    :param record: A PublishingRecord object.
    :param metadata: The json metadata of the record.
    """

    record.model.json = dict(metadata)
    flag_modified(record.model, 'json')
    db.session.merge(record.model)
    db.session.commit()


def delete_record(pid_type, pid_value):
    """Delete record from Publishing Invenio Instance

    :param pid_type: The type of pid(eg. art_id).
    :param pid_value: The value of PID.
    """
    try:
        record_to_delete = retrieve_record(pid_type, pid_value)
        
        # Remove buckets
        record_bucket = record_to_delete.files.bucket
        RecordsBuckets.query.filter_by(record_id=record_to_delete.id).delete()
        record_bucket.locked = False
        record_bucket.remove()

        # Remove the record from index
        try:
            RecordIndexer().delete(record_to_delete)
        except NotFoundError:
            pass

        try:
            doi = PersistentIdentifier.get('doi', record_to_delete['doi'])
            datacite_inactivate.delay(doi.pid_value, record_to_delete)
        except (PIDDoesNotExistError, KeyError):
            doi = None


        # Clear the record and put the deletion information
        record_to_delete.clear()
        record_to_delete.update({
            'titles':    
                {'title':'Record has been removed'},
            'reason': 'Reason',
        })
        record_to_delete.commit()

        db.session.commit()
    except PIDDoesNotExistError:
        print(
            'Record with pid {} does not exist.'.format(pid_value)
        )
    except AttributeError:
        print(
            'Record with pid {} has been already deleted.'.format(pid_value)
        )


def get_articles_per_section(record, section):
    """Return artcles per record"""
    if record.get('iss_id') and section:
        search = RecordsSearch(index='articles')
        default_filter = Q('bool', must=[Q('match', section=section), 
                                        Q('match', search_issue_id=record.get('iss_id'))])
        return search.query(default_filter)[:10].sort('sequence.article', '_search.keywords.title').execute()


def get_issues_for_journal(record):
    """Return artcles per record"""
    if record.get('jrn_id'):
        search = RecordsSearch(index='issues')
        default_filter = Q('bool', must=[Q('match', search_journal_id=record.get('jrn_id'))])
        return search.query(default_filter)[:5].sort('sequence.issue','-year','-volume').execute()


def get_journal_name(record):
    """Return Journal Name """

    pid_value = record.get('search_journal_id')
    try:
        id_ =  PersistentIdentifier.get('jrn_id', pid_value).object_uuid
        journal = PublishingRecord.get_record(id_)
        if journal.get('jrn_id'):
            return journal.get('titles').get('title')
        return current_app.config.get('THEME_FRONTPAGE_TITLE')
    except PIDDoesNotExistError:
        return current_app.config.get('THEME_FRONTPAGE_TITLE')


def get_issue_name(record):
    """Return Issue Name """

    pid_value = record.get('search_issue_id')
    try:
        id_ =  PersistentIdentifier.get('iss_id', pid_value).object_uuid
        journal = PublishingRecord.get_record(id_)
        return journal.get('titles').get('title')
    except PIDDoesNotExistError:
        return None


def get_issue_info(record, key):
    """Return Issue info """

    pid_value = record.get('search_issue_id')
    try:
        id_ =  PersistentIdentifier.get('iss_id', pid_value).object_uuid
        journal = PublishingRecord.get_record(id_)
        return journal.get(key)
    except PIDDoesNotExistError:
        return None


def record_exists(recordpid, recordtype):
    """Check if record exists"""
    try:
        obj = PersistentIdentifier.get(recordtype, recordpid)
        record = PublishingRecord.get_record(obj.object_uuid)
        if record.get(recordtype):
            return True
    except PIDDoesNotExistError:
        pass
    return False
    
