from __future__ import absolute_import, print_function

from invenio_pidstore.providers.base import BaseProvider
from invenio_pidstore.providers.datacite import DataCiteProvider
from invenio_pidstore.models import PIDStatus


class JournalProvider(BaseProvider):
    """Journal identifier provider"""

    pid_type = 'jrn_id'
    """Type of persistent identifier"""

    pid_provider = None
    """Provider name.
    The provides handles only the creation of issue ids.
    """

    default_status = PIDStatus.REGISTERED
    """Issue UUIDS are registered immediately."""


class IssueProvider(BaseProvider):
    """Issue identifier provider"""

    pid_type = 'iss_id'
    """Type of persistent identifier"""

    pid_provider = None
    """Provider name.
    The provides handles only the creation of issue ids.
    """

    default_status = PIDStatus.REGISTERED
    """Issue UUIDS are registered immediately."""


class ArticleProvider(BaseProvider):
    """Article identifier provider"""

    pid_type = 'art_id'
    """Type of persistent identifier"""

    pid_provider = None
    """Provider name.
    The provides handles only the creation of article ids.
    """

    default_status = PIDStatus.REGISTERED
    """Article UUIDS are registered immediately."""