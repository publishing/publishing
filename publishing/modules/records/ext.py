from __future__ import absolute_import, print_function, division

from invenio_indexer.signals import before_record_index
from .indexer import indexer_receiver
from .cli import journal



class PublishingRecords(object):
    """Publishing OAI Harvesting Ojs App"""

    def __init__(self, app=None):
        """Extension initialization."""
        if app:
            self.init_app(app)

    def init_app(self, app):
        """Flask application initialization."""
        app.extensions['publishing-records'] = self
        self.register_signals(app)
        app.cli.add_command(journal)


    @staticmethod
    def register_signals(app):
        """Register Publishing before index signals."""
        before_record_index.connect(indexer_receiver, sender=app)
