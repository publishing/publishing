from __future__ import absolute_import, print_function, unicode_literals

import datetime
import os
import json
import six

from flask import Blueprint, current_app, render_template, request
from operator import itemgetter
from werkzeug.utils import import_string

from invenio_previewer.proxies import current_previewer
from invenio_i18n.ext import current_i18n
from invenio_records_ui.signals import record_viewed
from publishing.modules.stats.utils import get_record_stats, generate_chart
from publishing.modules.theme.utils import get_cover_image, get_thumb_image
from publishing.modules.records.utils import get_articles_per_section,\
     get_issues_for_journal, get_journal_name, get_issue_name, \
     record_exists, get_issue_info

from .serializers import citeproc_v1
from .serializers.json import PublishingJSONSerializer



blueprint = Blueprint(
    'publishing_records',
    __name__,
    template_folder='templates'
    )



@blueprint.app_template_filter()
def select_preview_file(files):
    """Get list of files and select one for preview."""
    selected = None
    try:
        for f in sorted(files or [], key=itemgetter('key')):
            if f['type'] in current_previewer.previewable_extensions:
                if selected is None:
                    selected = f
                elif f['default']:
                    selected = f
    except KeyError:
        pass
    return selected


#
# Stats filters
#

@blueprint.app_template_filter()
def record_stats(record):
    """Fetch record statistics from Elasticsearch."""
    return get_record_stats(record.id, False)


@blueprint.app_template_filter()
def record_stats_chart(record):
    """Fetch record statistics from Elasticsearch."""
    pid_value =  record.get('art_id') or record.get('iss_id') or record.get('jrn_id')
    return generate_chart(pid_value, record['publication_date'])

@blueprint.app_template_filter()
def stats_num_format(num):
    """Format a statistics value."""
    return '{:,.0f}'.format(num or 0)


#
# Cover Image filters
#

@blueprint.app_template_filter()
def cover_image(record):
    """Create and Fetch record cover."""
    return get_cover_image(record)

@blueprint.app_template_filter()
def thumb_image(record_files):
    """Create and Fetch record cover."""
    return get_thumb_image(record_files)

#
# Date filters
#

@blueprint.app_template_filter()
def dateformat(record_date):
    """Format date."""
    record_date = record_date.replace(' GMT','')

    return  datetime.datetime.strptime(record_date, '%a, %d %b %Y %H:%M:%S').date()


#
# Section filters
#
@blueprint.app_template_filter()
def article_sections(section, record):
    """Format date."""
    return  get_articles_per_section(record, section)


#
# issue filters
#
@blueprint.app_template_filter()
def issue_title(record):
    """Issue Title."""
    return  get_issue_name(record)


@blueprint.app_template_filter()
def issue_info(record, value):
    """Issue Title."""
    return  get_issue_info(record, value)


@blueprint.app_template_filter()
def files_length(files):
    """Format date."""
    lenght = len(files)
    for file in files:
        if file['galley_type'] == "Cover":
            lenght-=1
    return  lenght


#
# Journal filters
#
@blueprint.app_template_filter()
def issues_for_journal(record):
    """Issues for journal."""
    return  get_issues_for_journal(record)


@blueprint.app_template_filter()
def journal_title(record):
    """Journal Title."""
    return  get_journal_name(record)


@blueprint.app_template_filter()
def html_template(pid, rec_type, ext):
    """Journal Template."""
    BASE_DIR = os.path.dirname(os.path.realpath(__file__))
    path = current_app.config.get('CUSTOM_TEMPLATES_PATH').\
            format(rec_type, pid, pid+ext)

    if os.path.exists(BASE_DIR + '/templates/' + path):
        return path
    elif rec_type == 'articles':
        return False
    else:
        return current_app.config.get('BASE_UI_TEMPLATE').\
            format(rec_type, ext)


@blueprint.app_template_filter()
def search_path(current_url, record):
    if not record_exists(record.get('search_journal_id'), 'jrn_id'):
        return '/search'
    if current_url.endswith('search') and not 'issues' in current_url:
        return current_url
    return '/journals/{}/search'.format(record.get('search_journal_id'))


@blueprint.app_template_filter()
def journal_path(recordpid):
    if record_exists(recordpid, 'jrn_id'):
        return '/journals/{}'.format(recordpid)
    return '/'
    


@blueprint.app_template_filter('citation')
def citation(record, pid, style=None, ln=None):
    """Render citation for record according to style and language."""
    locale = ln or current_i18n.language
    style = style or 'science'
    try:
        return citeproc_v1.serialize(pid, record, style=style, locale=locale)
    except Exception:
        current_app.logger.exception(
            'Citation formatting for record {0} failed.'
            .format(str(record.id)))
        return None 


def articles_ui_export(pid, record, template=None, **kwargs):
    """Record serialization view."""

    formats = current_app.config.get('RECORDS_EXPORTFORMATS')
    fmt = request.view_args.get('format')

    # if formats.get(fmt) is None:
    #     return render_template(
    #         ''), 410
    # else:
    serializer = import_string(formats[fmt]['serializer'])
    # Pretty print if JSON
    if isinstance(serializer, PublishingJSONSerializer):
        json_data = serializer.transform_record(pid, record)
        data = json.dumps(json_data, indent=2, separators=(', ', ': '))
    else:
        data = serializer.serialize(pid, record)
    if isinstance(data, six.binary_type):
        data = data.decode('utf8')

    # emit record_viewed event
    record_viewed.send(
        current_app._get_current_object(),
        pid=pid,
        record=record,
    )
    return render_template(
        template, pid=pid, record=record,
        data=data, format_code=fmt, format_title=formats[fmt]['title'])
