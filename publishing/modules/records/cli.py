"""Click command-line interface for record management."""

from __future__ import absolute_import, print_function

import json
import sys
import uuid
import warnings

import click
import pkg_resources
from flask import current_app
from flask.cli import with_appcontext
from invenio_db import db
from sqlalchemy import exc
from invenio_pidstore.models import PersistentIdentifier
from invenio_files_rest.models import Bucket
from invenio_indexer.api import RecordIndexer
from invenio_pidstore.errors import PIDDoesNotExistError

from publishing.modules.oaiharvester.utils import check_existence, generate_invenio_file_name
from publishing.modules.records.minters import publishing_doi_updater
from publishing.modules.records.utils import insert_record, create_file,\
    create_record_metadata, retrieve_record, update_invenio_json, update_files
from publishing.modules.datacite.tasks import datacite_register, update_datacite_metadata
from publishing.modules.records.utils import delete_record


def abort_if_false(ctx, param, value):
    """Abort command is value is False."""
    if not value:
        ctx.abort()


@click.group()
def journal():
    """ Command for loading fixture data """


@journal.command()
@click.argument('source', type=click.File('r'), default=sys.stdin)
@click.option('--force', is_flag=True, default=False)
@with_appcontext
def create(source,force):
    """Create new journal record(s)."""

    # Make sure that all imports are done with application context.
    indexer = RecordIndexer()

    data = json.load(source)

    if isinstance(data, dict):
        data = [data]

    for record in data:
        metadata = record.get('journal').get('metadata')
        files = record.get('journal').get('galleys')

        try:
            pid_value = metadata.get('jrn_id')
            obj = PersistentIdentifier.get('jrn_id', pid_value)

            if force:
                existing_record = retrieve_record('jrn_id', pid_value)
                existing_record = update_files(files,existing_record)
                update_invenio_json(existing_record, metadata)

                new_record = retrieve_record('jrn_id', pid_value)

                if '_files' in existing_record:
                    files_metadata = [file_metadata for file_metadata in existing_record['_files']]
                    new_record['_files'] = files_metadata
                
                new_record = new_record.commit()
                indexer.bulk_index([str(new_record.id)])
                
                db.session.commit()
            else:
                raise click.BadParameter(
                    'Record with id={0} already exists. If you want to '
                    'override its data use --force.'.format(pid_value),
                )
        except PIDDoesNotExistError:
            rec = insert_record('journals', metadata)

            if files:
                for file in files:
                    create_file(file, rec)


@journal.command()
@click.option('-i', '--pid', 'pids', multiple=True)
@click.option('--yes-i-know', is_flag=True, callback=abort_if_false,
              expose_value=False,
              prompt="Do you really want to delete a journal record? "
                "We strongly advise you against that. If you are sure about deleting a Journal,"
                "keep in mind that the journal's Issues and Articles, will not be deleted.")
@with_appcontext
def delete(pids):
    """Delete Journal record(s)."""

    from .api import PublishingRecord
    for pid_ in pids:
        try:
            delete_record('jrn_id', pid_)
        except PIDDoesNotExistError:
            raise click.BadParameter(
                    'Record with pid={0} does not exist.'.format(pid_),
                )