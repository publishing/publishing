"""Persistent identifier minters"""

from __future__ import absolute_import, print_function

import idutils
from flask import current_app

from invenio_db import db
from invenio_pidstore.models import PersistentIdentifier, PIDStatus
from .providers import JournalProvider, IssueProvider, ArticleProvider


def jrn_id_minter(record_uuid, data):
    """Publishing journal minter."""
    provider = JournalProvider.create(
        object_type='rec',
        pid_type='jrn_id',
        object_uuid=record_uuid,
        pid_value=data.get('jrn_id')
    ).pid
    data['jrn_id'] = provider.pid_value
    if data.get('doi'):
        publishing_doi_minter(record_uuid, data, 'jrn_id')
    return provider


def iss_id_minter(record_uuid, data):
    """Publishing issue minter."""
    provider = IssueProvider.create(
        object_type='rec',
        pid_type='iss_id',
        object_uuid=record_uuid,
        pid_value=data.get('iss_id')
    ).pid
    data['iss_id'] = provider.pid_value
    if data.get('doi'):
        publishing_doi_minter(record_uuid, data, 'iss_id')
    return provider


def art_id_minter(record_uuid, data):
    """Publishing article minter."""
    provider = ArticleProvider.create(
        object_type='rec',
        pid_type='art_id',
        object_uuid=record_uuid,
        pid_value=data.get('art_id')
    ).pid
    data['art_id'] = provider.pid_value
    if data.get('doi'):
        publishing_doi_minter(record_uuid, data, 'art_id')
    return provider


def publishing_doi_minter(record_uuid, data, pid_type):
    """Mint DOI."""
    doi = data.get('doi')
    assert pid_type in data

    # Make sure it's a proper DOI
    assert idutils.is_doi(doi)

    return PersistentIdentifier.create(
            'doi',
            doi,
            pid_provider='datacite',
            object_type='rec',
            object_uuid=record_uuid,
            status=PIDStatus.RESERVED,
        )


def publishing_doi_updater(record_uuid, data):
    """Update the DOI."""
    doi = data.get('doi')
    assert doi
    assert idutils.is_doi(doi)

    doi_pid = PersistentIdentifier.get_by_object(
        pid_type='doi', object_type='rec', object_uuid=record_uuid)

    if doi_pid.pid_value != doi:
        with db.session.begin_nested():
            db.session.delete(doi_pid)
            return PersistentIdentifier.create(
                'doi',
                doi,
                pid_provider='datacite',
                object_type='rec',
                object_uuid=record_uuid,
                status=PIDStatus.RESERVED,
            )
