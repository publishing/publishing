""" CLI for Publishing Fixtures """

from __future__ import absolute_import, print_function, unicode_literals

from os import makedirs
from os.path import exists

from flask import current_app
from invenio_db import db
from invenio_files_rest.models import Location



def loadlocations(force=False):
    """Load default file store and archive location."""
    try:
        locs = []
        uris = [
            ('default', True, current_app.config['FIXTURES_FILES_LOCATION'], ),
            ('archive', False,
             current_app.config['FIXTURES_ARCHIVE_LOCATION'], )
        ]
        for name, default, uri in uris:
            if uri.startswith('/') and not exists(uri):
                makedirs(uri)
            if not Location.query.filter_by(name=name).count():
                loc = Location(name=name, uri=uri, default=default)
                db.session.add(loc)
                locs.append(loc)

        db.session.commit()
        return locs
    except Exception:
        db.session.rollback()
        raise