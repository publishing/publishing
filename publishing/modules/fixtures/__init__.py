"""Fixtures module."""

from __future__ import absolute_import, print_function

from .ext import PublishingFixtures

__all__ = ('PublishingFixtures', )