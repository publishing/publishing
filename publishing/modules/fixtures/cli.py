""" CLI For Publishing Fixtures """

from __future__ import absolute_import, print_function

import json
from os.path import dirname, join

import click
from flask.cli import with_appcontext
from invenio_db import db

from .files import loadlocations
from .oai import loadoaiharvestconfig,  loadapiconfig

@click.group()
def fixtures():
    """ Command for loading fixture data """

@fixtures.command()
@with_appcontext
def init():
    """ Load basic data """
    loadlocations()
    loadoaiharvestconfig()
    loadapiconfig()
    click.secho('Publishing fixtures has been initialized', fg='green')


@fixtures.command('loadlocations')
@with_appcontext
def loadlocations_cli():
    """ Load data store location """
    locs = loadlocations()
    click.secho('Create location(s): {0}'.format(
        [loc.uri for loc in locs]), fg='green')


@fixtures.command('loadoaiharvestconfig')
@with_appcontext
def loadoaiharvestconfig_cli():
    """ Load OAI-PMH Harvest config """
    oaiharvestconfig = loadoaiharvestconfig()
    click.secho('Created {0} OAI-PMH Harvest config'.format(oaiharvestconfig), fg='green')

@fixtures.command('loadapiconfig')
@with_appcontext
def loadapiconfig_cli():
    """ Load Update from API config """
    apiconfig = loadapiconfig()
    click.secho('Created {0} OAI-PMH Harvest config'.format(apiconfig), fg='green')