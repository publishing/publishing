""" Fixtures for Publishing """

from __future__ import absolute_import, print_function

import sys
from os.path import join

from .cli import fixtures

class PublishingFixtures(object):
    
    def __init__(self, app=None):
        """ Initialize extension """
        if app:
            self.init_app(app)

    def init_app(self, app):
        """ Flask application initialization """
        self.init_config(app.config)
        app.cli.add_command(fixtures)

    def init_config(self, config):
        """ Flask application initialization """
        config.setdefault(
            'FIXTURES_FILES_LOCATION',
            join(sys.prefix, 'var/instance/data')
        )

        config.setdefault(
            'FIXTURES_ARCHIVE_LOCATION',
            join(sys.prefix, 'var/instance/archive')
        )
        