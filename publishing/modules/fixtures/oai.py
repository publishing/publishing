"""CLI for Publishing fixtures."""


from __future__ import absolute_import, print_function

from invenio_db import db
from invenio_oaiharvester.models import OAIHarvestConfig
from publishing.modules.records.models import APIConfig

def loadoaiharvestconfig():
    """Load default OAIHarvest Config."""
    oaiharvestconfigs=[
        ('https://ojs.web.cern.ch/index.php/index/oai','OJS OAI','marcxml', '1900-1-1','')
        ]

    try:
        for baseurl, name, metadataprefix, lastrun, setspecs in oaiharvestconfigs:
            oaiharvestconf=OAIHarvestConfig(baseurl=baseurl, name=name, metadataprefix=metadataprefix, 
                                                lastrun=lastrun,setspecs=setspecs)
            db.session.add(oaiharvestconf)
        
        db.session.commit()
        return len(oaiharvestconfigs)
    except Exception:
        db.session.rollback()
        raise

def loadapiconfig():
    """Load default UpdateFromAPI Config."""
    apiconfigs=[
        ('https://ojs-api.web.cern.ch/issues/update/', 'OJS Update', '1900-1-1 0:0:0')
        ]

    try:
        for baseurl, name, lastrun in apiconfigs:
            apiconf=APIConfig(baseurl=baseurl, name=name, 
                                lastrun=lastrun)
            db.session.add(apiconf)
        
        db.session.commit()
        return len(apiconfigs)
    except Exception:
        db.session.rollback()
        raise