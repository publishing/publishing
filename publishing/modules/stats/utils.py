"""Statistics utilities."""

import itertools
import datetime

from flask import request
from elasticsearch import TransportError
from elasticsearch_dsl import Q
from elasticsearch_dsl.aggs import Filter
from invenio_search.api import RecordsSearch
from invenio_stats import current_stats
from invenio_records_files.models import RecordsBuckets
from publishing.modules.records.api import PublishingRecord

def build_record_stats(pid_value):
    """Build the record's stats."""
    stats = {}
    stats_sources = {
        'record-view': {
            'params': {'pid_value': pid_value},
            'fields': {
                'views': 'count',
                'unique_views': 'unique_count',
            },
        },
        'file-download': {
            'params': {'pid_value': pid_value},
            'fields': {
                'downloads': 'count',
                'unique_downloads': 'unique_count',
                'volume': 'volume',
            },
        },
    }

    for query_name, cfg in stats_sources.items():
        try:
            query_cfg = current_stats.queries[query_name]
            query = query_cfg.cls(**query_cfg.params)
            result = query.run(**cfg['params'])
            for dst, src in cfg['fields'].items():
                stats[dst] = result.get(src)
        except Exception:
            pass
    return stats


def extract_event_record_metadata(record):
    """Extract from a record the payload needed for a statistics event."""
    return dict(
        pid_type=str(record.id),
        pid_value=str(record['art_id']) if record.get('art_id') else record['iss_id'],
    )


def get_record_from_context(**kwargs):
    """Get the cached record object from kwargs or the request context."""
    if 'record' in kwargs:
        return kwargs['record']
    else:
        if 'obj' in kwargs:
            bucket_id = str(kwargs['obj'].bucket_id)

            if bucket_id is not None:

                rbs = RecordsBuckets.query.filter_by(bucket_id=bucket_id).all()

                rb = next(iter(rbs), None)
                if rb:
                    record = PublishingRecord.get_record(rb.record_id)


            return record


def get_record_stats(recordid, throws=True):
    """Fetch record statistics from Elasticsearch."""
    try:
        # https://bit.ly/2Kpyquv override '_all' it breaks with prefix
        res = (RecordsSearch(index='*')  
               .source(includes='_stats')  # only include "_stats" field
               .get_record(recordid)
               .execute())
        return res[0]._stats.to_dict() if res else None
    except Exception:
        if throws:
            raise
        pass


def generate_chart(recordpid, publication_date, length=6):
    """Generate stats chart for record"""
    date = datetime.datetime.now()
    publication_date = datetime.datetime.strptime(publication_date, '%Y-%m-%d')
    time_passed = (date.replace(day=1)-publication_date.replace(day=1)).days//30
    if time_passed < length:
        length = time_passed + 1
    months = []
    views = []
    downloads = []
    year = str(date.year)
    month='{:02d}'.format(date.month)

    for mon in range(0,length):
        views.append(aggregate_stats_per_month('events-stats-record-view-{}-{}'
                                                .format(year, month), recordpid))

        downloads.append(aggregate_stats_per_month('events-stats-file-download-{}-{}'
                                                    .format(year, month), recordpid))
            
        months.append(datetime.date(2019,int(month),1).strftime("%B"))

        if month == '01':
            month = '12'
            year = str(date.year-1)
        else:
            month = '{:02d}'.format(int(month)-1)
    
    return views[::-1], downloads[::-1], months[::-1]


def aggregate_stats_per_month(index, recordpid):
    """Get stats per month"""
    try:
        query_agg = RecordsSearch(index=index)
        pid_agg = Filter(Q('term', pid_value=recordpid))
        pid_agg.bucket('views_stats', 'terms', field="pid_value")
        query_agg.aggs.bucket('views',pid_agg)

        response = query_agg.execute()
        if response:
            return response.aggregations.views.views_stats.buckets[0].doc_count
    except (TransportError, IndexError):
        return 0