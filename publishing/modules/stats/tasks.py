"""Tasks for statistics."""

from datetime import datetime

from celery import shared_task
from dateutil.parser import parse as dateutil_parse
from elasticsearch_dsl import Index, Search
from invenio_indexer.api import RecordIndexer
from invenio_pidstore.models import PersistentIdentifier
from invenio_pidstore.errors import PIDDoesNotExistError
from invenio_stats import current_stats
from invenio_stats.utils import get_bucket_size



@shared_task(ignore_result=True)
def update_record_statistics(start_date=None, end_date=None):
    """Update "_stats" field of affected records."""
    start_date = dateutil_parse(start_date) if start_date else None
    end_date = dateutil_parse(end_date) if start_date else None
    aggr_configs = {}

    if not start_date and not end_date:
        start_date = datetime.utcnow()
        end_date = datetime.utcnow()

        for aggr_name in current_stats.aggregations_config:
            aggr_cfg = current_stats.aggregations[aggr_name]
            aggr = aggr_cfg.cls(
                aggregation_name=aggr_cfg.name, **aggr_cfg.params)

            if not Index(aggr.aggregation_alias, using=aggr.client).exists():
                if not Index(aggr.event_index, using=aggr.client).exists():
                    start_date = min(start_date, datetime.utcnow())
                else:
                    start_date = min(
                        start_date, aggr.bookmark_api._get_oldest_event_timestamp())

            # Retrieve the last two bookmarks
            bookmarks = Search(
                using=aggr.bookmark_api.client,
                index=aggr.bookmark_api.bookmark_index,
            ).filter(
                'term', aggregation_type=aggr.bookmark_api.agg_type
            )[0:2].sort({'date': {'order': 'desc'}}).execute()

            if len(bookmarks) >= 1:
                end_date = max(
                    end_date,
                    datetime.strptime(bookmarks[0].date, aggr.doc_id_suffix))
            if len(bookmarks) == 2:
                start_date = min(
                    start_date,
                    datetime.strptime(bookmarks[1].date, aggr.doc_id_suffix))

            aggr_configs[aggr.aggregation_alias] = aggr
    elif start_date and end_date:
        for aggr_name in current_stats.enabled_aggregations:
            aggr_cfg = current_stats.aggregations[aggr_name]
            aggr = aggr_cfg.cls(
                name=aggr_cfg.name, **aggr_cfg.params)
            aggr_configs[aggr.aggregation_alias] = aggr
    else:
        return

    recordids = set()
    for aggr_alias, aggr in aggr_configs.items():
        query = Search(
            using=aggr.client,
            index=aggr.aggregation_alias,
            doc_type=aggr.aggregation_doc_type,
        ).filter(
            'range', timestamp={
                'gte': start_date.replace(microsecond=0).isoformat() + '||/d',
                'lte': end_date.replace(microsecond=0).isoformat() + '||/d'}
        ).extra(_source=False)
        query.aggs.bucket('ids', 'terms', field='pid_value', \
            size=get_bucket_size(aggr.client, aggr.event_index, aggr.aggregation_field))
        recordids |= {
            b.key for b in query.execute().aggregations.ids.buckets}

    indexer = RecordIndexer()
    types = ['jrn_id', 'iss_id', 'art_id']
    for recordid_value in recordids:
        pid_obj = None
        for type in types:
            try:
                pid_obj = PersistentIdentifier.get(type, recordid_value)
            except PIDDoesNotExistError:
                pass
        if pid_obj:
            indexer.bulk_index([str(pid_obj.object_uuid)])
