"""Statistics events builders."""


from .utils import extract_event_record_metadata, get_record_from_context



def add_record_metadata(event, sender_app, **kwargs):
    """Add Publishing-specific record fields to the event."""
    record = get_record_from_context(**kwargs)
    if record:
        event.update(extract_event_record_metadata(record))
    return event
