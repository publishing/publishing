from __future__ import absolute_import, print_function, division

import json
import warnings
from requests.exceptions import RetryError
from invenio_indexer.api import RecordIndexer
from invenio_db import db
from invenio_pidstore.errors import PIDDoesNotExistError, PIDAlreadyExists
from publishing.modules.records.minters import publishing_doi_updater
from publishing.modules.records.utils import insert_record, create_file,\
    create_record_metadata, retrieve_record, update_invenio_json, update_files

from publishing.modules.datacite.tasks import datacite_register, update_datacite_metadata
from .utils import check_existence, extract_ojs_id, generate_invenio_file_name,\
    update_oaiharvest_lastrun, update_oaiharvest_retry, is_valid_doi


def listener(sender, records, *args, **kwargs):
    """Post process function for articles, issues and journals"""
    if records:
        extract_articles_from_ojs(records)
    else:
        update_oaiharvest_retry()


def extract_articles_from_ojs(records):
    """Process to gather article metadata from OJS, create/update
    articles, extract OJS ids and trigger the issue workflow.
    
    :param records: list of harvested records.
    """
    try:
        articles_ojs_id = [extract_ojs_id(record) 
                        for record in records if not record.deleted]

        records_metadata = [create_record_metadata('articles', ojs_id)
                        for ojs_id in articles_ojs_id]

        records_to_create, records_to_update = check_existence('art_id',
                                                            records_metadata)

        for new_record in records_to_create:
            create_record_invenio('articles',new_record[0].get('art_id'), new_record)

        for update_record in records_to_update:
            update_record_invenio('art_id','articles', update_record)

        update_oaiharvest_retry()
    except RetryError:
        update_oaiharvest_lastrun()
        warnings.warn(
            'Requests to the API failed.'
        )



def extract_issues_from_ojs(issues_ojs_id):
    """Process to gather issues' metadata from OJS, using them 
    to create/update issue records.
    
    :param issues_ojs_id: A list of OJS records ids.
    """
    try:
        records_metadata = [create_record_metadata('issues', ojs_id)
                        for ojs_id in issues_ojs_id]

        records_to_create, records_to_update = check_existence('iss_id',
                                                            records_metadata)

        for new_record in records_to_create:
            create_record_invenio('issues',new_record[0].get('iss_id'), new_record)
        
        for update_record in records_to_update:
            update_record_invenio('iss_id','issues', update_record)
    except RetryError:
        warnings.warn(
            'Requests to the API failed.'
        )


def create_record_invenio(type, pid_value, record):
    """Create Record for invenio instance workflow.
    
    :param type: Type of record.
    :param record: metadata of record.
    """

    metadata = record[0]
    files = record[2]

    try:
        rec = insert_record(type, metadata)

        if files:
            for file in files:
                create_file(file, rec)
        
        # DATACITE call to register DOI in datacite org
        doi =  metadata.get('doi')
        journal = metadata.get('search_journal_id')
        if doi and is_valid_doi(doi, journal):
            datacite_register.delay(type, pid_value, str(rec.id))

    except PIDAlreadyExists:
        pass



def update_record_invenio(pid_type, type, record):
    """Update Record for invenio instance workflow.
    
    :param pid_type: The type of the pid.
    :param record: A list with metadata of record.
    """

    metadata = record[0]
    files = record[2]
    indexer = RecordIndexer()

    try:
        existing_record = retrieve_record(pid_type, metadata[pid_type])
        existing_record = update_files(files,existing_record)
        update_invenio_json(existing_record, metadata)

        new_record = retrieve_record(pid_type, metadata[pid_type])

        # Add '_files' in the updated record.
        if '_files' in existing_record:
            files_metadata = [file_metadata for file_metadata in existing_record['_files']]
            new_record['_files'] = files_metadata
        

        new_record = new_record.commit()
        indexer.bulk_index([str(new_record.id)])
        
        db.session.commit()

        # Update doi if changed (invenio)
        doi = new_record.get('doi')
        journal = new_record.get('search_journal_id')
        if doi and is_valid_doi(doi, journal):
            publishing_doi_updater(new_record.id, new_record)
            update_datacite_metadata(doi, type, str(new_record.id), metadata[pid_type])
    except PIDDoesNotExistError:
        warnings.warn(
            'Record with pid={0} does not exist.'.format(metadata[pid_type]),
        )