"""OAI harvest database models."""

from __future__ import absolute_import, print_function

import datetime

from invenio_db import db


class OAIHarvestConfig(db.Model):
    """Represents a PublishingOAIHarvestConfig record.
    Note: This class is being used only inside Publishing Instance,
    the invenio_oaiharvester is using its own model. Be careful when
    you import they only refer to the same db table.
    """

    __tablename__ = 'oaiharvester_configs'
    __table_args__ = {'extend_existing': True} 

    id = db.Column(db.Integer, primary_key=True)
    baseurl = db.Column(db.String(255), nullable=False, server_default='')
    metadataprefix = db.Column(db.String(255), nullable=False,
                               server_default='oai_dc')
    comment = db.Column(db.Text, nullable=True)
    name = db.Column(db.String(255), nullable=False)
    lastrun = db.Column(db.DateTime, default=datetime.datetime(
        year=1900, month=1, day=1
    ), nullable=True)
    retry_lastrun = db.Column(db.DateTime, default=datetime.datetime(
        year=1900, month=1, day=1
    ), nullable=True)
    setspecs = db.Column(db.Text, nullable=False)

    def save(self):
        """Save object to persistent storage."""
        with db.session.begin_nested():
            db.session.merge(self)

    def update_lastrun(self, new_date=None):
        """Update the 'lastrun' attribute of object to now."""
        self.lastrun = new_date or datetime.datetime.now()
    
    def update_retry(self, new_date=None):
        """Update the 'retry_lastrun' attribute of object to new date."""
        self.retry_lastrun = new_date

__all__ = ('OAIHarvestConfig',)
