from __future__ import absolute_import, print_function, division

from invenio_oaiharvester.signals import oaiharvest_finished

from .process import listener


class PublishingHarvesterApp(object):
    """Publishing OAI Harvesting Ojs App"""

    def __init__(self, app=None):
        """Extension initialization."""
        if app:
            self.init_app(app)

    def init_app(self, app):
        """Flask application initialization."""
        app.extensions['publishing-oai-harvest'] = self
        self.register_signals(app)

    @staticmethod
    def register_signals(app):
        """Register Publishing OAI harvesting signals."""
        oaiharvest_finished.connect(listener)
