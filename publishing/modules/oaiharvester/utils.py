from __future__ import absolute_import, print_function, division

import json
import datetime
import requests

from flask import current_app
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from dojson.contrib.marc21.utils import create_record
from invenio_db import db
from invenio_pidstore.models import PersistentIdentifier
from invenio_pidstore.errors import PIDDoesNotExistError
from publishing.modules.dojson.articles import articles
from .models import OAIHarvestConfig as PublishingOAIHarvestConfig


def check_existence(pid_type, records):
    """Check if a record exists in the invenio instance
    and returns records to create and update.
    
    :param pid_type: A pid type (e.g art_id).
    :param records: A list of records.
    :return create: A list of records.
    :return update: A list of records.
    """

    create = []
    update = []

    for record in records:
        # Get invenio_pid
        pid_value=record[0][pid_type]

        try:
            obj = PersistentIdentifier.get(pid_type, pid_value)
            update.append(record)
        except PIDDoesNotExistError:
            create.append(record)

    return create, update
             

def request_record(type, ojs_id):
    """Request record from OJS API.
    
    :param type: The type of record(eg. articles).
    :param ojs_id: The ojs id of record.
    :return: JSON object.
    """
    url = current_app.config.get('OJS_API')+'{}/{}'.format(type, ojs_id)
    response = requests_retry_session().get(url)

    return response.json()



def extract_ojs_id(record):
    """Extract OJS id from OAI DC metadata.
    
    :param record: Harvest record .
    :return: OJS id.
    """

    article = articles.do(create_record(record.raw))
    
    if 'ojs_article' in article:
        return article['ojs_article'].split('/')[-1]


def generate_invenio_file_name(file):
    """Generate the invenio file name using original file name and
    ojs file id for versioning.

    :param file: File of the record.
    :return: A filename.
    """

    if 'file_id' in file:
        return str(file['file_id']) + '_' + file['file_name']
    else:
        return '1_'+file['file_name']


def get_records_api(type):
    """Request all records id from OJS API.

    :param type: Type of record.
    :return: A list with ojs_ids.
    """
    url = current_app.config.get('OJS_API')+'{}/'.format(type)

    response = requests_retry_session().get(url)
    response_json = response.json()

    ojs_ids = []

    for rec in response_json['data']:
        ojs_ids.append(rec['id'])

    return ojs_ids


def requests_retry_session(
    retries=3,
    backoff_factor=0.8,
    status_forcelist=(500, 503, 502, 504),
    session=None,
):
    """Retry requests session"""
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


def update_oaiharvest_retry():
    """Update retry datetime to keep the last successful run"""

    name = current_app.config.get('CELERY_BEAT_SCHEDULE'). \
            get('harvester').get('kwargs').get('name')
    
    oai_source = PublishingOAIHarvestConfig.query.filter_by(name=name).first()
    oai_source.update_retry(oai_source.lastrun)
    oai_source.save()
    db.session.commit()


def update_oaiharvest_lastrun():
    """Update lastrun to the last successful run"""
    
    name = current_app.config.get('CELERY_BEAT_SCHEDULE'). \
            get('harvester').get('kwargs').get('name')
    
    oai_source = PublishingOAIHarvestConfig.query.filter_by(name=name).first()
    oai_source.update_lastrun(oai_source.retry_lastrun)
    oai_source.save()
    db.session.commit()


def is_valid_doi(doi, journal=None):
    """Check if DOI is a valid managed DOI."""
    
    if not doi:
        return False
    
    prefixes = [
        current_app.config['PIDSTORE_DATACITE_DOI_PREFIX']
    ]

    if journal:
        api = current_app.config.get('DATACITE_CLIENT_API').get(journal)
        if api:
            prefix = api.get('prefix','')
            if prefix and prefix not in prefixes:
                prefixes = prefix if isinstance(prefix,list) else prefixes.append(prefix)

    for p in prefixes:
        if doi.startswith('{0}/'.format(p)):
            return True
    return False