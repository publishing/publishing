# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 CERN.
#
# publishing is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.

"""Blueprint used for loading templates.

The sole purpose of this blueprint is to ensure that Invenio can find the
templates and static files located in the folders of the same names next to
this file.
"""

from __future__ import absolute_import, print_function

from flask import Blueprint,  render_template, request, current_app
from elasticsearch_dsl import Q

from invenio_search.api import RecordsSearch

blueprint = Blueprint(
    'publishing_frontpage',
    __name__,
    template_folder='templates',
    url_prefix='',
)


@blueprint.route('/ping', methods=['HEAD', 'GET'])
def ping():
    """Load balancer ping view."""
    return 'OK'



@blueprint.route('/')
def index():
    """Frontpage blueprint."""

    return render_template(
        'publishing_frontpage/frontpage.html'
        )


#
# Section filters
#
@blueprint.app_template_filter()
def record_journal(search_journal_id):
    """ES query to retrieve journal by ID."""

    search = RecordsSearch(index='journals')
    default_filter = Q('bool', must=[Q('match', search_journal_id=search_journal_id)])
    return search.query(default_filter).execute()