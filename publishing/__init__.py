# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 CERN.
#
# publishing is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.

"""publishing."""

from __future__ import absolute_import, print_function

from .version import __version__

__all__ = ('__version__', )
