# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 CERN.
#
# publishing is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.

"""Default configuration for publishing.

You overwrite and set instance-specific configuration by either:

- Configuration file: ``<virtualenv prefix>/var/instance/invenio.cfg``
- Environment variables: ``APP_<variable name>``
"""

from __future__ import absolute_import, print_function

import os
from datetime import timedelta
from celery.schedules import crontab

from invenio_search import RecordsSearch

from invenio_app.config import APP_DEFAULT_SECURE_HEADERS
from invenio_records_rest.facets import terms_filter
from invenio_search import current_search_client
from invenio_stats.aggregations import StatAggregator
from invenio_stats.processors import EventsIndexer
from invenio_stats.queries import ESDateHistogramQuery, ESTermsQuery



def _(x):
    """Identity function used to trigger string extraction."""
    return x


# Rate limiting
# =============
#: Storage for ratelimiter.
RATELIMIT_STORAGE_URL = 'redis://localhost:6379/3'

# I18N
# ====
#: Default language
BABEL_DEFAULT_LANGUAGE = 'en'
#: Default time zone
BABEL_DEFAULT_TIMEZONE = 'Europe/Zurich'
#: Other supported languages (do not include the default language in list).
I18N_LANGUAGES = [
    # ('fr', _('French'))
]

# Base templates
# ==============
#: Global base template.
BASE_TEMPLATE = "publishing_theme/page.html"
#: Cover page base template (used for e.g. login/sign-up).
COVER_TEMPLATE = 'invenio_theme/page_cover.html'
#: Footer base template.
FOOTER_TEMPLATE = 'invenio_theme/footer.html'
#: Header base template.
HEADER_TEMPLATE = 'publishing_theme/header.html'
#: Settings base template.
SETTINGS_TEMPLATE = 'invenio_theme/page_settings.html'

# Theme configuration
# ===================
#: Site name
THEME_SITENAME = _('CERN Publishing')
#: Use default frontpage.
THEME_FRONTPAGE = False
#: Frontpage title.
THEME_FRONTPAGE_TITLE = _('CERN Publishing')

THEME_SITEURL = "https://publishing.cern.ch"

THEME_ERROR_TEMPLATE = 'publishing_theme/errors/page_error.html'
THEME_404_TEMPLATE = 'publishing_theme/errors/404.html'
THEME_500_TEMPLATE = 'publishing_theme/errors/500.html'


# Email configuration
# ===================
#: Email address for support.
SUPPORT_EMAIL = "info@inveniosoftware.org"
#: Disable email sending by default.
MAIL_SUPPRESS_SEND = True

# Assets
# ======
#: Static files collection method (defaults to copying files).
COLLECT_STORAGE = 'flask_collect.storage.file'

# Accounts
# ========
#: Email address used as sender of account registration emails.
SECURITY_EMAIL_SENDER = SUPPORT_EMAIL
#: Email subject for account registration emails.
SECURITY_EMAIL_SUBJECT_REGISTER = _(
    "Welcome to publishing!")
#: Redis session storage URL.
ACCOUNTS_SESSION_REDIS_URL = 'redis://localhost:6379/1'


# Application
# ===========
#: Disable Content Security Policy headers.
APP_DEFAULT_SECURE_HEADERS['content_security_policy'] = {}
APP_HEALTH_BLUEPRINT_ENABLED = True

#: Allowed tags used for html sanitizing by bleach.
ALLOWED_HTML_TAGS = [
    'a',
    'abbr',
    'acronym',
    'b',
    'blockquote',
    'br',
    'code',
    'em',
    'i',
    'li',
    'ol',
    'p',
    'pre',
    'strike',
    'strong',
    'sub',
    'sup',
    'u',
    'ul',
]

#: Allowed attributes used for html sanitizing by bleach.
ALLOWED_HTML_ATTRS = {
    '*': [''],
    'a': ['href', 'title', 'name', 'rel', 'target'],
    'abbr': ['title'],
    'acronym': ['title'],
}


# Celery configuration
# ====================

BROKER_URL = 'amqp://guest:guest@localhost:5672/'
#: URL of message broker for Celery (default is RabbitMQ).
CELERY_BROKER_URL = 'amqp://guest:guest@localhost:5672/'
#: URL of backend for result storage (default is Redis).
CELERY_RESULT_BACKEND = 'redis://localhost:6379/2'
#: Scheduled tasks configuration (aka cronjobs).
CELERY_BEAT_SCHEDULE = {
    'harvester': {
        'task': 'invenio_oaiharvester.tasks.list_records_from_dates',
        'schedule': timedelta(minutes=10),
        'kwargs': {
            'name': 'OJS OAI',  
        }, },
    'update_issues': {
        'task': 'publishing.modules.records.tasks.update_issues_from_api',
        'schedule': timedelta(minutes=10),
        'kwargs': {
            'name': 'OJS Update',  
        }, },
    'delete-articles-records': {
        'task': 'publishing.modules.records.tasks.delete_article_records',
        'schedule': crontab(minute=0, hour=2),
    },
    'delete-issues-records': {
        'task': 'publishing.modules.records.tasks.delete_issue_records',
        'schedule': crontab(minute=0, hour=2),
    },
    'indexer': {
        'task': 'invenio_indexer.tasks.process_bulk_queue',
        'schedule': timedelta(minutes=5),
    },
    # # Stats
    'stats-process-events': {
        'task': 'invenio_stats.tasks.process_events',
        'schedule': timedelta(minutes=30),
        'args': [('record-view', 'file-download')],
    },
    'stats-aggregate-events': {
        'task': 'invenio_stats.tasks.aggregate_events',
        'schedule': timedelta(hours=3),
        'args': [(
            'record-view-agg', 'file-download-agg',
        )],
    },
    'stats-update-record-statistics': {
        'task': 'publishing.modules.stats.tasks.update_record_statistics',
        'schedule': crontab(minute=0, hour=1),  # Every day at 01:00 UTC
    },

}

# Database
# ========
#: Database URI including user and password
SQLALCHEMY_DATABASE_URI = \
    'postgresql+psycopg2://publishing:publishing@localhost/publishing'
# INVENIO_

INVENIO_SQLALCHEMY_DATABASE_URI = \
    'postgresql+psycopg2://publishing:publishing@localhost/publishing'
# JSONSchemas
# ===========
#: Hostname used in URLs for local JSONSchemas.
JSONSCHEMAS_HOST = 'publishing.cern.ch'
JSONSCHEMAS_REPLACE_REFS = True

JSONSCHEMAS_RESOLVE_SCHEMA = True

# Flask configuration
# ===================
# See details on
# http://flask.pocoo.org/docs/0.12/config/#builtin-configuration-values

#: Secret key - each installation (dev, production, ...) needs a separate key.
#: It should be changed before deploying.
SECRET_KEY = 'CHANGE_ME'
#: Max upload size for form data via application/mulitpart-formdata.
MAX_CONTENT_LENGTH = 100 * 1024 * 1024  # 100 MiB
#: Sets cookie with the secure flag by default
SESSION_COOKIE_SECURE = True
#: Since HAProxy and Nginx route all requests no matter the host header
#: provided, the allowed hosts variable is set to localhost. In production it
#: should be set to the correct host and it is strongly recommended to only
#: route correct hosts to the application.
APP_ALLOWED_HOSTS = ['localhost', '127.0.0.1', 'publishing.cern.ch']

# OAI-PMH
# =======
OAISERVER_ID_PREFIX = 'oai:publishing.cern.ch:'

# Debug
# =====
# Flask-DebugToolbar is by default enabled when the application is running in
# debug mode. More configuration options are available at
# https://flask-debugtoolbar.readthedocs.io/en/latest/#configuration

#: Switches off incept of redirects by Flask-DebugToolbar.
DEBUG_TB_INTERCEPT_REDIRECTS = False
DEBUG_TB_ENABLED=False

SQLALCHEMY_TRACK_MODIFICATIONS = False


RECORDS_EXPORTFORMATS = {
    'hx': dict(
        title='BibTeX',
        serializer='publishing.modules.records.serializers.bibtex_v1',
    ),
    'xd': dict(
        title='Dublin Core',
        serializer='publishing.modules.records.serializers.dc_v1',
    ),
    'xm': dict(
        title='MARC21 XML',
        serializer='publishing.modules.records.serializers.marcxml_v1',
    ),
}


# Endpoints for displaying publishing records.
RECORDS_UI_ENDPOINTS = dict(
    jrn_id=dict(
        pid_type='jrn_id',
        route='/journals/<pid_value>',
        template='publishing_records/journal_body.html',
        record_class='publishing.modules.records.api:PublishingRecord',
    ),
    jrn_id_search=dict(
        pid_type='jrn_id',
        route='/journals/<pid_value>/search',
        template='publishing_search_ui/journal_search.html',
        record_class='publishing.modules.records.api:PublishingRecord',
    ),
    jrn_id_archives=dict(
        pid_type='jrn_id',
        route='/journals/<pid_value>/archives',
        template='publishing_search_ui/journal_archives.html',
        record_class='publishing.modules.records.api:PublishingRecord',
    ),
    iss_id=dict(
        pid_type='iss_id',
        route='/issues/<pid_value>',
        template='publishing_records/issue_body.html',
        record_class='publishing.modules.records.api:PublishingRecord'
    ),
    iss_id_files=dict(
        pid_type='iss_id',
        route='/issues/<pid_value>/files/<filename>',
        view_imp='invenio_records_files.utils.file_download_ui',
        record_class='publishing.modules.records.api:PublishingRecord',
    ),
    iss_id_preview=dict(
        pid_type='iss_id',
        route='/issues/<pid_value>/preview/<filename>',
        view_imp='invenio_previewer.views:preview',
        record_class='publishing.modules.records.api:PublishingRecord',
    ),
    iss_id_search=dict(
        pid_type='iss_id',
        route='/issues/<pid_value>/search',
        template='publishing_search_ui/issue_search.html',
        record_class='publishing.modules.records.api:PublishingRecord',
    ),
    art_id=dict(
        pid_type='art_id', 
        route='/articles/<pid_value>',
        template='publishing_records/articles/article_detail.html',
        record_class='publishing.modules.records.api:PublishingRecord'
    ),
    art_id_preview=dict(
        pid_type='art_id',
        route='/articles/<pid_value>/preview/<filename>',
        view_imp='invenio_previewer.views:preview',
        record_class='publishing.modules.records.api:PublishingRecord',
    ),
    art_id_files=dict(
        pid_type='art_id',
        route='/articles/<pid_value>/files/<filename>',
        view_imp='invenio_records_files.utils.file_download_ui',
        record_class='publishing.modules.records.api:PublishingRecord',
    ),
    art_id_file=dict(
        pid_type='art_id',
        route='/articles/<pid_value>/files',
        template='publishing_records/articles/article_files.html',
        record_class='publishing.modules.records.api:PublishingRecord',
    ),
    art_id_stats=dict(
        pid_type='art_id',
        route='/articles/<pid_value>/statistics',
        template='publishing_records/articles/article_statistics.html',
        record_class='publishing.modules.records.api:PublishingRecord',
    ),
    art_id_export=dict(
        pid_type='art_id',
        route='/articles/<pid_value>/export/<any({0}):format>'.format(", ".join(
            list(RECORDS_EXPORTFORMATS.keys()))),
        template='publishing_records/articles/article_export.html',
        view_imp='publishing.modules.records.views.articles_ui_export',
        record_class='publishing.modules.records.api:PublishingRecord',
    ),
)

RECORDS_REST_ENDPOINTS = dict(
    jrn_id=dict(
        pid_type='jrn_id',
        pid_minter='journals',
        pid_fetcher='journal_fetcher',
        search_class=RecordsSearch,
        search_index='journals',
        search_type='journals',
        record_serializers={
             'application/json': (
                'publishing.modules.records.serializers.json_v1_response'),
        },
        search_serializers={
            'application/json': (
                'publishing.modules.records.serializers:json_v1_search'),
        },
        list_route='/journals/',
        item_route='/journals/<pid(jrn_id):pid_value>',
        default_media_type='application/json',
        max_result_window=10000,
    ),
    iss_id=dict(
        pid_type='iss_id',
        pid_minter='issues',
        pid_fetcher='issue_fetcher',
        search_class=RecordsSearch,
        search_index='issues-issues',
        search_type='issues',
        record_serializers={
             'application/json': (
                'publishing.modules.records.serializers.json_v1_response'),
        },
        search_serializers={
            'application/json': (
                'publishing.modules.records.serializers:json_v1_search'),
        },
        list_route='/issues/',
        item_route='/issues/<pid(iss_id):pid_value>',
        default_media_type='application/json',
        max_result_window=10000,
    ),
    art_id=dict(
        pid_type='art_id',
        pid_minter='articles',
        pid_fetcher='article_fetcher',
        search_class=RecordsSearch,
        search_index='articles-articles',
        search_type='articles',
        record_serializers={
             'application/json': (
                'publishing.modules.records.serializers.json_v1_response'),
            'application/vnd.citationstyles.csl+json': (
                'publishing.modules.records.serializers.csl_v1_response'),
            'text/x-bibliography': (
                'publishing.modules.records.serializers.citeproc_v1_response'),
            'application/x-bibtex': (
                'publishing.modules.records.serializers.bibtex_v1_response'),
            'application/x-dc+xml': (
                'publishing.modules.records.serializers.dc_v1_response'),
        },
        search_serializers={
            'application/json': (
                'publishing.modules.records.serializers:json_v1_search'),
            'application/x-bibtex': (
                'publishing.modules.records.serializers:bibtex_v1_search'),
            'application/x-dc+xml': (
                'publishing.modules.records.serializers.dc_v1_search'),

        },
        list_route='/articles/',
        item_route='/articles/<pid(art_id):pid_value>',
        default_media_type='application/json',
        max_result_window=10000,
    ),
    combined=dict(
        pid_type='combined',
        pid_minter='issues',
        pid_fetcher='combined_fetcher',
        search_class=RecordsSearch,
        search_index='articles-articles,issues-issues',
        search_type=None,
        # links_factory_imp=('publishing.modules.records.links:'
        #    'combined_links_factory'),
        record_serializers={
             'application/json': (
                'publishing.modules.records.serializers.json_v1_response'),
        },
        search_serializers={
            'application/json': (
                'publishing.modules.records.serializers:json_v1_search'),
        },
        list_route='/combined/',
        item_route='/combined/<pid(combined):pid_value>',
        default_media_type='application/json',
        max_result_window=10000,
    ),
    records=dict(
        pid_type='combined',
        pid_minter='issues',
        pid_fetcher='combined_fetcher',
        search_class=RecordsSearch,
        search_index='articles-articles,issues-issues,journals-journals',
        search_type=None,
        # links_factory_imp=('publishing.modules.records.links:'
        #    'combined_links_factory'),
        record_serializers={
             'application/json': (
                'publishing.modules.records.serializers.json_v1_response'),
        },
        search_serializers={
            'application/json': (
                'publishing.modules.records.serializers:json_v1_search'),
        },
        list_route='/records/',
        item_route='/records/<pid(combined):pid_value>',
        default_media_type='application/json',
        max_result_window=10000,
    ),
)

#: Defined facets for records REST API.
RECORDS_REST_FACETS = {
    'articles-articles,issues-issues': {
        'aggs': dict(
            Authors= dict(
                terms={'field': 'authors.fullname'}
            ),
            Keywords = dict(
                terms={'field': 'keywords'}
            ),
        ),
        'post_filters':dict(
            search_journal_id=terms_filter('search_journal_id'),
            Authors=terms_filter('authors.fullname'),
            Keywords=terms_filter('keywords')
        ),
        'filters': dict(
            search_journal_id=terms_filter('search_journal_id'),
            Authors=terms_filter('authors.fullname'),
            Keywords=terms_filter('keywords')
        ),
    },
    'articles-articles,issues-issues,journals-journals': {
        'aggs': dict(
            Journal= dict(
                terms={'field': 'search_journal_id'}
            ),
            Authors= dict(
                terms={'field': 'authors.fullname'}
            ),
            Keywords= dict(
                terms={'field': 'keywords'}
            ),
        ),
        'post_filters':dict(
            Journal=terms_filter('search_journal_id'),
            Authors=terms_filter('authors.fullname'),
            Keywords=terms_filter('keywords')
        ),
        'filters': dict(
            Journal=terms_filter('search_journal_id'),
            Keywords=terms_filter('keywords'),
            Authors=terms_filter('authors.fullname'),
        ),
    },
    'articles-articles': {
        'aggs': dict(
            Section= dict(
                terms={'field': '_search.keywords.section'}
            ),
            Authors= dict(
                terms={'field': 'authors.fullname'}
            ),
            Keywords= dict(
                terms={'field': 'keywords'}
            ),
        ),
        'post_filters':dict(
            Section=terms_filter('_search.keywords.section'),
            Authors=terms_filter('authors.fullname'),
            Keywords=terms_filter('keywords')
        ),
        'filters': dict(
            search_issue_id=terms_filter('search_issue_id'),
            Section=terms_filter('_search.keywords.section'),
            Authors=terms_filter('authors.fullname'),
            Keywords=terms_filter('keywords')
        ),
    },
    'issues-issues': {
        'post_filters':dict(
            Journal=terms_filter('search_journal_id'),
        ),
        'filters': dict(
            search_journal_id=terms_filter('search_journal_id'),
        ),
    },
}

# #: Sort options records REST API.
RECORDS_REST_SORT_OPTIONS = {
    "articles-articles,issues-issues": {
        'bestmatch': dict(
           title='Best match',
           fields=['_score'],
           order=1,
       ),
        'date': dict(fields=['publication_date'],
            title='Date',
            default_order='asc',
            order=2),
        'title': dict(fields=['_search.keywords.title'],
            title='Title',
            default_order='asc',
            order=3)
    },
    "articles-articles,issues-issues,journals-journals": {
        'bestmatch': dict(
           title='Best match',
           fields=['_score'],
           order=1,
       ),
        'date': dict(
            fields=['publication_date'],
            title='Date',
            default_order='asc',
            order=2),
        'title': dict(
            fields=['_search.keywords.title'],
            title='Title',
            default_order='asc',
            order=3)
    },
    "issues-issues": {
        'bestmatch': dict(
           title='Best match',
           fields=['_score'],
           order=1,
       ),
        'date': dict(
            fields=['publication_date'],
            title='Date',
            default_order='asc',
            order=1),
        'title': dict(
            fields=['_search.keywords.title'],
            title='Title',
            default_order='asc',
            order=3)
    },
    "articles-articles": {
        'bestmatch':dict(
           title='Best match',
           fields=['_score'],
           order=1,
        ),
        'date': dict(
            fields=['publication_date'],
            title='Date',
            default_order='asc',
            order=2),
        'title': dict(
            fields=['_search.keywords.title'],
            title='Title',
            default_order='asc',
            order=3),
        'sequence': dict(
            fields=['sequence.section','sequence.article'],
            title='Sequence',
            default_order='asc',
            order=4)
    }
}

RECORDS_FILES_REST_ENDPOINTS = {}

FILES_REST_PERMISSION_FACTORY = \
    'publishing.modules.records.permissions.files_permission_factory'

#: Default sort for records REST API.
RECORDS_REST_DEFAULT_SORT = {
    'issues-issues': dict(query='-bestmatch', noquery='date'),
    'articles-articles,issues-issues,journals-journals': dict(
        query='-bestmatch',
        noquery='-bestmatch'
    ),
    'articles-articles,issues-issues': dict(query='-bestmatch'),
    'articles-articles': dict(query='-bestmatch', noquery='sequence'),
}

# Journal UI
#======

BASE_UI_TEMPLATE = "publishing_records/{}/base{}.html"
CUSTOM_TEMPLATES_PATH = "publishing_records/{}/{}/{}.html"

# Search
#======
SEARCH_UI_SEARCH_API = '/api/combined/'

SEARCH_UI_SEARCH_TEMPLATE = "publishing_search_ui/global_search.html"
SEARCH_UI_RESULTS_TEMPLATE = "publishing_search_ui/search.html"
SEARCH_UI_JSTEMPLATE_FACETS = 'templates/publishing_search_ui/facets.html'

SEARCH_UI_SEARCH_INDEX_JOURNAL = 'articles-articles,issues-issues'
SEARCH_UI_SEARCH_INDEX_GLOBAL = 'articles-articles,issues-issues,journals-journals'
SEARCH_UI_SEARCH_INDEX_ISSUE = 'articles-articles'

SEARCH_MAPPINGS = ['journals', 'issues', 'articles']

SEARCH_INDEX_PREFIX = 'prod-'

SEARCH_MAPPINGS_EXTRA_PARAMS = [
    dict(
        object_path_from=("titles", "title"),
        object_path_to=("keywords", "title")
    ),
    dict(
        object_path_from=("section",),
        object_path_to=("keywords", "section")
    ),
]


es_user = os.environ.get('ELASTICSEARCH_USER')
es_password = os.environ.get('ELASTICSEARCH_PASSWORD')
if es_user and es_password:
    es_params = dict(
        http_auth=(es_user, es_password),
        use_ssl=str(os.environ.get('ELASTICSEARCH_USE_SSL')).lower() == 'true',
        verify_certs=str(
            os.environ.get('ELASTICSEARCH_VERIFY_CERTS')).lower() == 'true',
        url_prefix=os.environ.get('ELASTICSEARCH_URL_PREFIX', ''),
    )
else:
    es_params = {}

SEARCH_ELASTIC_HOSTS = [
    dict(
        host=os.environ.get('ELASTICSEARCH_HOST', 'localhost'),
        port=int(os.environ.get('ELASTICSEARCH_PORT', '9200')),
        **es_params
    )
]

# Fixtures
#=======
FIXTURES_FILES_LOCATION= '/opt/invenio/var/instance/data'
FIXTURES_ARCHIVE_LOCATION= '/opt/invenio/var/instance/archived'

# Stats
#=======

STATS_EVENTS ={
    'file-download': {
        'signal': 'invenio_files_rest.signals.file_downloaded',
        'templates':'publishing.modules.stats.templates.events',
        'event_builders': [
            'invenio_stats.contrib.event_builders.file_download_event_builder',
            'publishing.modules.stats.event_builders:add_record_metadata',
        ],
        'cls': EventsIndexer,
        'params': {
            'preprocessors': [
                'invenio_stats.processors:flag_robots',
                # Don't index robot events
                lambda doc: doc if not doc['is_robot'] else None,
                'invenio_stats.processors:flag_machines',
                'invenio_stats.processors:anonymize_user',
                'invenio_stats.contrib.event_builders:build_file_unique_id',
            ],
            # Keep only 1 file download for each file and user every 30 sec
            'double_click_window': 30,
            # Create one index per month which will store file download events
            'suffix': '%Y-%m',
        },
    },
    'record-view': {
        'signal': 'invenio_records_ui.signals.record_viewed',
        'templates':'publishing.modules.stats.templates.events',
        'event_builders': [
            'invenio_stats.contrib.event_builders.record_view_event_builder',
        ],
        'cls': EventsIndexer,
        'params': {
            'preprocessors': [
                'invenio_stats.processors:flag_robots',
                # Don't index robot events
                lambda doc: doc if not doc['is_robot'] else None,
                'invenio_stats.processors:flag_machines',
                'invenio_stats.processors:anonymize_user',
                'invenio_stats.contrib.event_builders:build_record_unique_id',
            ],
            # Keep only 1 file download for each file and user every 30 sec
            'double_click_window': 30,
            # Create one index per month which will store file download events
            'suffix': '%Y-%m',
        },
    },
    
}

#: Enabled aggregations from 'publishing.modules.stats.registrations'
STATS_AGGREGATIONS = {
    'file-download-agg': dict(
        aggregation_name='file-download-agg',
        templates='publishing.modules.stats.templates.aggregations',
        cls=StatAggregator,
        params=dict(
            client=current_search_client,
            event='file-download',
            aggregation_field='pid_value',
            aggregation_interval='day',
            batch_size=1,
            copy_fields=dict(
                bucket_id='bucket_id',
                pid_value='pid_value',
                pid_type='pid_type',
                size='size',
                country='country'
            ),
            metric_aggregation_fields=dict(
                unique_count=('cardinality', 'unique_session_id',
                              {'precision_threshold': 1000}),
                volume=('sum', 'size', {}),
            )
        )
    ),
    'record-view-agg': dict(
        templates='publishing.modules.stats.templates.aggregations',
        cls=StatAggregator,
        params=dict(
            client=current_search_client,
            event='record-view',
            aggregation_field='pid_value',
            aggregation_interval='day',
            batch_size=1,
            copy_fields=dict(
                record_id='record_id',
                pid_value='pid_value',
                pid_type='pid_type',
                country='country'
            ),
            metric_aggregation_fields=dict(
                unique_count=('cardinality', 'unique_session_id',
                              {'precision_threshold': 1000}),
            )
        )
    ),
}


STATS_QUERIES = {
    'record-view': dict(
            cls=ESTermsQuery,
            params=dict(
                query_name='record-view',
                index='stats-record-view',
                copy_fields=dict(
                    record_id='record_id',
                    pid_value='pid_value',
                    pid_type='pid_type',
                    country='country'
                ),
                required_filters=dict(
                    pid_value='pid_value',
                ),
                metric_fields=dict(
                    count=('sum', 'count', {}),
                    unique_count=('sum', 'unique_count', {}),
                )
            )
        ),
    'file-download': dict(
            cls=ESTermsQuery,
            params=dict(
                query_name='file-download',
                index='stats-file-download',
                copy_fields=dict(
                    bucket_id='bucket_id',
                    pid_value='pid_value',
                    pid_type='pid_type',
                    size='size',
                    country='country'
                ),
                required_filters=dict(
                    pid_value='pid_value',
                ),
                metric_fields=dict(
                    count=('sum', 'count', {}),
                    unique_count=('sum', 'unique_count', {}),
                    volume=('sum', 'volume', {}),
                )
            ),
        ),
}

# Sentry  
# ======

# SENTRY_DSN = 'https://user:pw@sentry.example.org/'



# Piwik
# ======
THEME_PIWIK_ID = None


# DataCite
# ========

#: DataCite API - URL endpoint.
PIDSTORE_DATACITE_URL = os.environ.get('DATACITE_URL',"https://mds.test.datacite.org")
#: DataCite API - Disable test mode.
PIDSTORE_DATACITE_TESTMODE = False
#: Datacite test prefix 
PIDSTORE_DATACITE_DOI_PREFIX = os.environ.get('DATACITE_DOI_PREFIX','10.70111')
#:Clients per Journal.
DATACITE_CLIENT_API = dict(
    CYRSP=dict(
        username=os.environ.get('DATACITE_CYRSP_USERNAME',''),
        password=os.environ.get('DATACITE_CYRSP_PASSWORD','CHANGE_ME'),
        prefix=os.environ.get('DATACITE_CYRSP_DOI_PREFIX','10.70111')
    ),
    CP=dict(
        username=os.environ.get('DATACITE_CP_USERNAME',''),
        password=os.environ.get('DATACITE_CP_PASSWORD','CHANGE_ME'),
        prefix=os.environ.get('DATACITE_CP_DOI_PREFIX','10.70111')
    ),
)


#: List of previewers (adds IIIF previewer).
PREVIEWER_PREFERENCE = [
    'csv_dthreejs',
    'iiif_image',
    'simple_image',
    'json_prismjs',
    'xml_prismjs',
    'mistune',
    'pdfjs',
    'ipynb',
    'zip',
]

# IIIF
# ====
#: Improve quality of image resampling using better algorithm
IIIF_RESIZE_RESAMPLE = 'PIL.Image:BICUBIC'


PUBLISHING_RECORDS_UI_LINKS_FORMAT = 'https://publishing.cern.ch/{type}/{pid_value}'

OJS_API='https://ojs-api.web.cern.ch/'
PROVENANCE_SOURCES = 'OJS 3'



# CSL Citation Formatter
# ======================
#: Styles Endpoint for CSL
CSL_STYLES_API_ENDPOINT = '/api/csl/styles'
#: Records Endpoint for CSL
CSL_RECORDS_API_ENDPOINT = '/api/articles/'
#: Template dirrectory for CSL
CSL_JSTEMPLATE_DIR = 'node_modules/invenio-csl-js/dist/templates/'
#: Template for CSL citation result
CSL_JSTEMPLATE_CITEPROC = 'templates/invenio_csl/citeproc.html'
#: Template for CSL citation list item
CSL_JSTEMPLATE_LIST_ITEM = 'templates/invenio_csl/item.html'
#: Template for CSL citation select item
CSL_JSTEMPLATE_SELECT_ITEM = 'templates/invenio_csl/select.html'
#: Template for CSL error
CSL_JSTEMPLATE_ERROR = 'templates/invenio_csl/error.html'
#: Template for CSL loading
CSL_JSTEMPLATE_LOADING = 'templates/invenio_csl/loading.html'
#: Template for CSL typeahead
CSL_JSTEMPLATE_TYPEAHEAD = 'templates/invenio_csl/typeahead.html'
