# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 CERN.
#
# publishing is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.

"""Version information for publishing.

This file is imported by ``publishing.__init__``,
and parsed by ``setup.py``.
"""

from __future__ import absolute_import, print_function

__version__ = '1.0.0'
