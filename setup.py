# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 CERN.
#
# publishing is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.

"""Invenio digital library framework."""

import os

from setuptools import find_packages, setup

readme = open('README.rst').read()

DATABASE = "postgresql"
ELASTICSEARCH = "elasticsearch7"
INVENIO_VERSION = "3.2.0a4"

tests_require = [
    'check-manifest>=0.35',
    'coverage>=4.4.1',
    'isort>=4.3',
    'mock>=2.0.0',
    'pydocstyle>=2.0.0',
    'pytest-cov>=2.5.1',
    'pytest-invenio>=1.0.2,<1.1.0',
    'pytest-mock>=1.6.0',
    'pytest-pep8>=1.0.6',
    'pytest-random-order>=0.5.4',
    'pytest>=3.3.1',
    'selenium>=3.4.3',
]

extras_require = {
    'docs': [
        'Sphinx>=1.5.1',
    ],
    'tests': tests_require,
}

extras_require['all'] = []
for reqs in extras_require.values():
    extras_require['all'].extend(reqs)

setup_requires = [
    'Babel>=2.4.0',
    'pytest-runner>=3.0.0,<5',
]

install_requires = [
    'arrow>=0.13.0',
    'amqp>=2.5.0',
    'bleach>=3.1.0',
    'celery>4.1.0,<=4.1.1',
    'citeproc-py-styles>=0.1.1',
    'citeproc-py>=0.4.0',
    'datacite>=1.0.1',
    'dcxml>=0.1.1',
    'dojson>=1.4.0',
    'Flask>=1.1.1',
    'Flask-BabelEx>=0.9.3',
    'Flask-Debugtoolbar>=0.10.1',
    'Flask-SQLAlchemy>=2.4.0',
    'invenio[{db},{es},base,files,auth,metadata]~={version}'.format(
        db=DATABASE, es=ELASTICSEARCH, version=INVENIO_VERSION),
    'invenio-access>=1.1.0',
    'invenio-accounts>=1.1.1',
    'invenio-admin>=1.1.1',
    'invenio-app>=1.2.0',
    'invenio-assets>=1.1.3',
    'invenio-base>=1.1.0',
    'invenio-cache>=1.0.0',
    'invenio-celery>=1.1.0',
    'invenio-config>=1.0.2',
    'invenio-csl-rest>=1.0.0a1',
    'invenio-db>=1.0.4',
    'invenio-files-rest>=1.0.1',
    'invenio-formatter>=1.0.2',
    'invenio-i18n>=1.1.1',
    'invenio-iiif>=1.0.0',
    'invenio-indexer>=1.1.0',
    'invenio-jsonschemas>=1.0.0',
    'invenio-logging>=1.2.0',
    'invenio-mail>=1.0.2',
    'invenio-marc21>=1.0.0a9',
    'invenio-oaiharvester>=1.0.0a4',
    'invenio-oaiserver>=1.0.3',
    'invenio-oauth2server>=1.0.3',
    'invenio-oauthclient>=1.1.3',
    'invenio-pidstore>=1.0.0',
    'invenio-previewer>=1.0.1',
    'invenio-queues>=1.0.0a1',
    'invenio-records>=1.3.0',
    'invenio-records-files>=1.1.1',
    'invenio-records-rest>=1.5.0',
    'invenio-records-ui>=1.0.1',
    'invenio-rest[cors]>=1.1.1',
    'invenio-search>=1.2.1',
    'invenio-search-ui>=1.1.1',
    'invenio-theme>=1.1.4',
    'invenio-userprofiles>=1.0.1',
    'IDUtils>=1.1.2',
    'python-slugify>=3.0.1',
    'pycountry>=18.12.8',
    'uWSGI>=2.0.17',
]

packages = find_packages()


# Get the version string. Cannot be done with import!
g = {}
with open(os.path.join('publishing', 'version.py'), 'rt') as fp:
    exec(fp.read(), g)
    version = g['__version__']

setup(
    name='publishing',
    version=version,
    description=__doc__,
    long_description=readme,
    keywords='cern publishing Invenio',
    license='MIT',
    author='CERN',
    author_email='info@inveniosoftware.org',
    url='https://gitlab.cern.ch/publishing/publishing',
    packages=packages,
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    entry_points={
        'console_scripts': [
            'publishing = invenio_app.cli:cli',
        ],
        'invenio_base.blueprints': [
            'publishing_frontpage = publishing.modules.frontpage.views:blueprint',
            'publishing_records = publishing.modules.records.views:blueprint',
            'publishing_search_ui = publishing.modules.search_ui.views:blueprint',
            'publishing_theme = publishing.modules.theme.views:blueprint',
        ],
        'invenio_jsonschemas.schemas': [
            'publishing_records = publishing.modules.records.jsonschemas',
        ],
        'invenio_pidstore.minters': [
            'journals = publishing.modules.records.minters:jrn_id_minter',
            'issues = publishing.modules.records.minters:iss_id_minter',
            'articles = publishing.modules.records.minters:art_id_minter'
        ],
        'invenio_pidstore.fetchers': [
            'journal_fetcher = publishing.modules.records.fetchers:journal_fetcher',
            'issue_fetcher = publishing.modules.records.fetchers:issue_fetcher',
            'article_fetcher = publishing.modules.records.fetchers:article_fetcher',
            'combined_fetcher = publishing.modules.records.fetchers:combined_fetcher',
        ],
        'invenio_search.mappings': [
            'journals = publishing.modules.records.mappings',
            'issues = publishing.modules.records.mappings',
            'articles = publishing.modules.records.mappings'
        ],
        'invenio_celery.tasks': [
            'publishing_records = publishing.modules.records.tasks',
            'publishing_stats = publishing.modules.stats.tasks',
        ],
        'invenio_config.module': [
            'publishing = publishing.config',
        ],
        'invenio_db.models': [
            'publishing_oaiharvester = publishing.modules.oaiharvester.models',
        ],
        'invenio_i18n.translations': [
            'messages = publishing',
        ],
        'invenio_base.apps': [
            'publishing_harvester = publishing.modules.oaiharvester.ext:PublishingHarvesterApp',
            'publishing_fixtures = publishing.modules.fixtures.ext:PublishingFixtures',
            'publishing_records = publishing.modules.records.ext:PublishingRecords'
        ],
        'invenio_assets.webpack':[
            'publishing_theme = publishing.modules.theme.webpack:theme',
            'charts_js = publishing.modules.theme.webpack:stats_js',
            'search_js = publishing.modules.theme.webpack:search_js',
            'citation_js = publishing.modules.theme.webpack:citation_js'
        ],
        'dojson.contrib.to_marc21': [
            'publishing = publishing.modules.records.serializers.to_marc21.rules',
        ],
    },
    extras_require=extras_require,
    install_requires=install_requires,
    setup_requires=setup_requires,
    tests_require=tests_require,
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Development Status :: 3 - Alpha',
    ],
)
